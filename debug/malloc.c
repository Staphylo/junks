#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "malloc.h"
#include "malloc_undef.h"

typedef struct malloc_list_s *malloc_list_t;
struct malloc_list_s
{
    const char    *file;
    const char    *func;
    int            line;
    size_t         size;
    void          *ptr;
    malloc_list_t  next;
};

static malloc_list_t list = NULL;
static malloc_list_t last = NULL;
static int num_malloc = 0;
static int num_free = 0;

void *
memdb_malloc(const char *func, const char *file, int line, size_t size)
{
    malloc_list_t node = malloc(sizeof(struct malloc_list_s));

    node->file = file;
    node->func = func;
    node->line = line;
    node->size = size;
    node->ptr  = malloc(size);
    node->next = NULL;

    num_malloc++;

    if(!node->ptr)
        fprintf(stderr, "Malloc failed at %s:%d %s\n", file, line, func);

    if(!list)
        list = node;
    if(last)
        last->next = node;
    last = node;

    return node->ptr;
}

void
memdb_free(const char *func, const char *file, int line, void *ptr)
{
    malloc_list_t tmp  = list;
    malloc_list_t prev = list;

    if(!ptr)
    {
        fprintf(stderr, "%s:%d %s"__MP__" => freeing NULL pointer\n",
                file, line, func);
        return;
    }

    while(tmp && tmp->ptr != ptr)
    {
        prev = tmp;
        tmp = tmp->next;
    }

    if(tmp)
    {
        if(prev == tmp)
            list = tmp->next;
        else
            prev->next = tmp->next;
        if(tmp == last)
        {
            if(tmp == prev)
                last = NULL;
            else
                last = prev;
        }
        free(ptr);
        free(tmp);
        num_free++;
    }
    else
    {
        fprintf(stderr, "%s:%d %s"__MP__" => %p not found in allocated blocks",
                file, line, func, ptr);
    }
}

void
memdb_init(int flags __attribute__((unused)))
{
}

void
memdb_cleanup()
{
    int           i   = 0;
    malloc_list_t tmp = list;
    if(list)
        fputs("===== MEMORY CLEANUP =====\n", stderr);
    while(tmp)
    {
        i++;
        fprintf(stderr, "%s:%d %s"__MP__" => %zu bytes not freed at %p\n",
                tmp->file, tmp->line, tmp->func, tmp->size, tmp->ptr);
        free(tmp->ptr);
        list = tmp;
        tmp = tmp->next;
        free(list);
    }

    list = NULL;
    last = NULL;

    if(i)
    {
        fprintf(stderr, "%d block still allocated\n", i);
        fprintf(stderr, "%d free for %d malloc\n", num_free, num_malloc);
    }
}
