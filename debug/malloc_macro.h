#define malloc(Size) \
    malloc_logger(__FUN__, __FILE__, __LINE__, Size)

#define free(Ptr) \
    free_logger(__FUN__, __FILE__, __LINE__, Ptr)

