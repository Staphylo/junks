/*
** ----------------------------------------------------------------------------
** "THE BEER-WARE LICENSE" (Revision 42):
** <staphyloa@gmail.com> wrote this file. As long as you retain this notice you
** can do whatever you want with this stuff. If we meet some day, and you think
** this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
** ----------------------------------------------------------------------------
**
** Created by Samuel 'Staphylo' Angebault on Tue Apr 17
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "debug.h"
#include "tweak.h"
#include "tweak_private.h"
#include "malloc_undef.h"

const char *tweak_type_str[] =
{
    [TWEAK_INT]    = "int",
    [TWEAK_FLOAT]  = "float",
    [TWEAK_DOUBLE] = "double",
    [TWEAK_CHAR]   = "char",
    [TWEAK_STRING] = "string"
};

static tweak_val_t  tweaks = NULL;
static tweak_file_t files  = NULL;

// actually the previous strings are not freed in order to be always available
static char *
atos(const char *str)
{
    const char *begin;
    char *out, *res;
    size_t len = 0;

    if(*str != '"')
        return NULL;
    str++;

    begin = str;
    while(*str != '"')
    {
        if(*str == '\\')
            str++;
        else
            len++;
        str++;
    }

    out = malloc((len + 1) * sizeof(char));
    res = out;
    str = begin;
    while(*str != '"')
    {
        if(*str == '\\')
            str++;
        *out = *str;
        out++;
        str++;
    }
    *out = '\0';

    return res;//strndup(begin, str-begin);
}

static char
atoc(const char *str)
{
    /*if(*(str++) != ''')
        return 0;*/
    if(*(++str) == '\\')
        return *(++str);
    return *str;
}

// searching the tweak value in the table. if not there returning -1
// result untouched if the return value is -1
static int
tweak_find_value(const char *file, const size_t counter, tweak_data_u *result)
{
    tweak_val_t v = tweaks;
    while(v)
    {
        if(v->counter == counter)
        {
            if(strcmp(v->file, file) == 0)
            {
                *result = v->value;
                return 0;
            }
        }
        v = v->next;
    }
    return -1;
}

// function returning -1 if the file is not found
static int
tweak_find_file(const char *file)
{
    tweak_file_t f = files;
    while(f)
    {
        if(strcmp(f->file, file) == 0)
            return 0;
    }
    return -1;
}

// return the modification time of the given file
static time_t
tweak_file_mtime(const char *file)
{
    struct stat s;
    stat(file, &s);
    return s.st_mtime;
}

// add a new file if it doesn't exist yet
static void
tweak_add_file(const char *file)
{
    tweak_file_t f = malloc(sizeof(struct tweak_file_s));
    f->file = file;
    f->next = files;
    f->atime = tweak_file_mtime(file);
    files = f;
}

// adding a new tweak in the list
static void
tweak_add_value(const char *file, const size_t counter, tweak_data_u value,
                tweak_type_e type)
{
    tweak_val_t v = malloc(sizeof(struct tweak_val_s));
    v->file = file;
    v->counter = counter;
    v->type = type;
    v->value = value;
    v->next = tweaks;
    tweaks = v;
}

// return the value corresponding, or adding the default one
tweak_data_u
tweak_value(const char *file, const size_t counter, tweak_data_u value,
            tweak_type_e type)
{
    tweak_data_u result;
    if(tweak_find_value(file, counter, &result) != -1)
        return result;
    if(tweak_find_file(file) == -1)
        tweak_add_file(file);
    tweak_add_value(file, counter, value, type);
    return value;
}

// update the given file
static void
tweak_update_value(const char *file, const size_t counter, tweak_data_u value,
                   tweak_type_e type)
{
    tweak_val_t v = tweaks;
    while(v)
    {
        if(v->counter == counter && strcmp(v->file, file) == 0)
        {
            if(v->type != type)
                fprintf(stderr, "type of the constant changed from %s to %s\n",
                        tweak_type_str[v->type], tweak_type_str[type]);
            else
                v->value = value;
            return;
        }
        v = v->next;
    }
    tweak_add_value(file, counter, value, type);
}

#define GET_VALUE(Field, Func, Type) \
    c += 2;                          \
    value.Field = Func(c);           \
    type = TWEAK_##Type;             \
    break

// read the content of the file and search the tweak constants
static void
tweak_update_file(const char *file, FILE *fp)
{
    char buffer[128]; // if it reach the limit you are not on 80 cols =p
    char *c;
    int comment = 0;
    size_t counter = 0;
    tweak_data_u value;
    tweak_type_e type;
    while(fgets(buffer, 128, fp))
    {
        c = buffer;
        while(*c)
        {
            if(*c == '/' && *(c+1) == '/')
                break;
            if(comment)
            {
                if(*c == '*' && *(c+1) == '/')
                {
                    comment = 0;
                    c += 2;
                    continue;
                }
            }
            else
            {
                if(*c == '/' && *(c+1) == '*')
                {
                    comment = 1;
                    c += 2;
                    continue;
                }

                if(strncmp(c, "_TV", 3) == 0)
                {
                    c += 3;
                    switch(*c)
                    {
                        case 'I': GET_VALUE(i, atoi, INT);
                        case 'F': GET_VALUE(f, atof, FLOAT);
                        case 'D': GET_VALUE(d, atof, DOUBLE);
                        case 'C': GET_VALUE(c, atoc, CHAR);
                        case 'S': GET_VALUE(s, atos, STRING);
                        default: continue;
                    }
                    /*fprintf(stderr, "modifying value %zu with %d\n", counter,
                            value.i);*/
                    tweak_update_value(file, counter, value, type);
                    while(*(++c) != ')');
                    ++counter;
                }
            }
            c++;
        }
    }
}

#undef GET_VALUE

// refresh the constants from their file if change
void
tweak_refresh()
{
    tweak_file_t f = files;
    FILE *fp = NULL;
    time_t mtime;
    while(f)
    {
        mtime = tweak_file_mtime(f->file);
        if(f->atime < mtime)
        {
            /*fprintf(stderr, "updating %s\n", f->file);*/
            fp = fopen(f->file, "rt");
            if(fp)
            {
                f->atime = mtime;
                tweak_update_file(f->file, fp);
                fclose(fp);
            }
        }
        f = f->next;
    }
}

// free the tweaks that were allocated
void
tweak_free()
{
    tweak_val_t v;
    tweak_file_t f;
    while(tweaks)
    {
        v = tweaks;
        tweaks = v->next;
        free(v);
    }
    while(files)
    {
        f = files;
        files = f->next;
        free(f);
    }
}

