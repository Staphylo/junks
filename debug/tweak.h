/*
** ----------------------------------------------------------------------------
** "THE BEER-WARE LICENSE" (Revision 42):
** <staphyloa@gmail.com> wrote this file. As long as you retain this notice you
** can do whatever you want with this stuff. If we meet some day, and you think
** this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
** ----------------------------------------------------------------------------
**
** Created by Samuel 'Staphylo' Angebault on Tue Apr 17
*/

/*
** Only work with filesystem mounted with mtime at true (default)
*/

#ifndef TWEAK_H
#define TWEAK_H

typedef enum tweak_type_e {
    TWEAK_INT     = 0,
    TWEAK_FLOAT   = 1,
    TWEAK_DOUBLE  = 2,
    TWEAK_CHAR    = 3,
    TWEAK_STRING  = 4
} tweak_type_e;

typedef union tweak_data_u
{
    char    c;
    int     i;
    float   f;
    double  d;
    char   *s;
    void   *p;
} tweak_data_u;


#ifdef DEBUG
void tweak_refresh();
void tweak_free();
tweak_data_u tweak_value(const char *file, const size_t counter,
                         tweak_data_u value, tweak_type_e type);

# define _TV(Val, Type) \
    tweak_value(__FILE__, __COUNTER__, (tweak_data_u)Val, Type)
# define _TVI(Val) _TV(Val, TWEAK_INT).i
# define _TVF(Val) _TV(Val, TWEAK_FLOAT).f
# define _TVD(Val) _TV(Val, TWEAK_DOUBLE).d
# define _TVC(Val) _TV(Val, TWEAK_CHAR).c
# define _TVS(Val) _TV(Val, TWEAK_STRING).s
#else
# define _TV(Val, Type) Val
# define _TVI(Val) Val
# define _TVF(Val) Val
# define _TVD(Val) Val
# define _TVC(Val) Val
# define _TVS(Val) Val
# define tweak_refresh()
# define tweak_free()
#endif /* DEBUG */


#endif /* !TWEAK_H */
