#ifndef MALLOC_H
#define MALLOC_H

#ifndef __FUN__
# ifdef COMPLEX_FUNC_NAME
#  define __FUN__ __PRETTY_FUNCTION__
#  define __MP__ ""
# else
#  define __FUN__ __FUNCTION__
#  define __MP__ "()"
# endif /* COMPLEX_FUNC_NAME */
#endif /* !__FUN__ */


#ifdef DEBUG

void  memdb_init(int flags);
void *memdb_malloc(const char *func, const char *file, int line, size_t size);
void  memdb_free(const char *func, const char *file, int line, void *ptr);
void  memdb_cleanup();
#include "malloc_def.h"

#else

# define memdb_init(Flags)
# define memdb_cleanup()

#endif /* DEBUG */


#endif /* !MALLOC_H */
