#ifndef DEBUG_H
#define DEBUG_H

#define COMPLEX_FUNC_NAME

#include "tweak.h"
#include "malloc.h"

#define debug_init()     \
    memdb_init(0);

#define debug_update()   \
    tweak_refresh();

/*
#define debug_dump(File) \
    tweak_dump(File);    \
    memdb_dump(File);    \
*/

#define debug_exit()     \
    tweak_free();        \
    memdb_cleanup();

#endif /* !DEBUG_H */
