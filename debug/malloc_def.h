#define malloc(Size) \
    memdb_malloc(__FUN__, __FILE__, __LINE__, Size)

#define free(Ptr) \
    memdb_free(__FUN__, __FILE__, __LINE__, Ptr)

