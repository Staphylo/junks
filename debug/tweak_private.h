#ifndef TWEAK_PRIVATE_H
#define TWEAK_PRIVATE_H

#include "tweak.h"

typedef struct tweak_val_s *tweak_val_t;
struct tweak_val_s {
    tweak_val_t   next;
    const char   *file;
    size_t        counter;
    tweak_type_e  type;
    tweak_data_u  value;
};

typedef struct tweak_file_s *tweak_file_t;
struct tweak_file_s {
    tweak_file_t  next;
    const char   *file;
    time_t        atime;
};

#endif /* !TWEAK_PRIVATE_H */
