/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <staphyloa@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define VERSION "0.42a"
#define DEFAULT_SORTING "folder,file"

#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))

/* structure containing some information on a file */
typedef struct fileinfos fileinfos_s;
typedef fileinfos_s* fileinfos_t;
struct fileinfos {
    uint32_t lines;
    uint32_t chars;
    uint32_t words;
    char *name;
    char *path;
};

/* list of the files */
typedef struct list list_s;
typedef list_s* list_t;
struct list {
    fileinfos_t item;
    list_t next;
    list_t prev;
};

/* extension list struct */
typedef struct ext_s {
    char *ext;
    size_t len;
    struct ext_s *next;
} *ext_t;

/* sorting list struct */
typedef struct sort_s {
    int sort;
    int order;
    struct sort_s *next;
} *sort_t;

/* configuration structure */
typedef struct {
    char *folder;
    int full;
    sort_t sorting;
    ext_t exts;
    ext_t black;
    ext_t white;
} conf_s;

/* enumeration for sorting */
enum {
    SORT_FOLDER,
    SORT_FILE,
    SORT_LINES,
    SORT_WORDS,
    SORT_CHARS
};

/* enumeration for ordering */
enum {
    ORDER_ASC   = 0,
    ORDER_DESC  = 1
};

/* variables */
list_t filelist = NULL;
list_t lastitem = NULL;
conf_s config = {
    .folder = NULL,
    .full = 0,
    .sorting = NULL,
    .exts = NULL,
    .black = NULL,
    .white = NULL
};

static void
list_add(fileinfos_t infos)
{
    list_s *new = malloc(sizeof(list_s));
    new->item = infos;
    new->next = NULL;
    if(!filelist)
    {
        filelist = new;
        new->prev = NULL;
    }
    else
    {
        lastitem->next = new;
        new->prev = lastitem;
    }
    lastitem = new;
}

static void
fileinfos_clean(fileinfos_t infos)
{
    free(infos->name);
    free(infos->path);
}

static void
list_clean()
{
    list_t it = filelist;
    list_t tmp;
    while(it)
    {
        tmp = it;
        it = it->next;
        fileinfos_clean(tmp->item);
        free(tmp->item);
        free(tmp);
    }
    filelist = NULL;
    lastitem = NULL;
}

static void
extlist_clean(ext_t list)
{
    ext_t tmp;
    while(list)
    {
        tmp = list;
        list = list->next;
        free(tmp->ext);
        free(tmp);
    }
}

static void
sortlist_clean(sort_t list)
{
    sort_t tmp = list;
    while(list)
    {
        tmp = list;
        list = list->next;
        free(tmp);
    }
}

static void
config_clean()
{
    if(config.folder)
        free(config.folder);
    sortlist_clean(config.sorting);
    extlist_clean(config.exts);
    extlist_clean(config.black);
    extlist_clean(config.white);
}

/* help message display */
static void
display_help(const char *prg)
{
    printf(
    "\n"
    "Usage   : %1$s [options] folder\n"
    "Example : %1$s -e c,h -w Makefile -o folder,file=desc .\n"
    "\n"
    "Options:\n"
    "\n"
    "   -o --order <opt>      : Ordering information such as\n"
    "                           key=value,key=value with:\n"
    "                           key   : folder, file, lines, words, chars\n"
    "                           value : asc/+, desc/- (default asc)\n"
    "   -s --sort <opt>       : Alias for the upper option\n"
    "   -f --full-path        : Display the full path of the folders\n"
    "   -b --blacklist <ext>  : Blacklist specified extensions\n"
    "   -e --extension <ext>  : Specified extensions to use\n"
    "   -w --whitelist <file> : Files to whitelist (such as Makefile)\n"
    "\n"
    "   -h --help             : Display this help message\n"
    "   -v --version          : Display version information\n"
    "\n"
    , prg);
}

/* version message display */
static void
display_version()
{
    puts(
        "\n"
        "       Name : FancyCount\n"
        "    Version : " VERSION "\n"
        "      Built : " __DATE__ ", " __TIME__ "\n"
        "  Copyright : Copyleft\n"
        "    License : Beerware\n"
    );
}

static int
parse_extensions(const char *str, ext_t *list)
{
    ext_t tmp;
    size_t i = 0;

    while(42)
    {
        while(str[i] != ',' && str[i] != '\0')
            ++i;

        tmp = malloc(sizeof(struct ext_s));
        tmp->next = *list;
        tmp->ext = strndup(str, i);
        tmp->len = i;
        *list = tmp;

        str += i+1;
        i = 0;

        if(str[i-1] == '\0')
            break;
    }

    return 0;
}

static int
parse_sorting_order(const char *str)
{
    if(strcmp(str, "asc") == 0 || strcmp(str, "+") == 0)
        return ORDER_ASC;
    if(strcmp(str, "desc") == 0 || strcmp(str, "-") == 0)
        return ORDER_DESC;
    return -1;
}

static int
parse_sorting(char *str, sort_t *list)
{
    static char *const token[] = {
        [SORT_FOLDER]   = "folder",
        [SORT_FILE]     = "file",
        [SORT_LINES]    = "lines",
        [SORT_WORDS]    = "words",
        [SORT_CHARS]    = "chars",
        NULL
    };

    char *value;
    int opt;
    sort_t node;

    while (*str != '\0')
    {
        opt = getsubopt(&str, token, &value);
        switch (opt)
        {
            case SORT_FOLDER:
            case SORT_FILE:
            case SORT_LINES:
            case SORT_WORDS:
            case SORT_CHARS:
                node = malloc(sizeof(struct sort_s));
                node->sort = opt;
                if (value == NULL)
                    node->order = ORDER_ASC;
                else
                    node->order = parse_sorting_order(value);
                node->next = *list;
                *list = node;
                /*node->next = NULL;
                if(tmp)
                    tmp->next = node;
                else
                    *list = node;
                tmp = node;*/
                if(node->order == -1)
                    return 1;
                break;
            default:
                fprintf(stderr, "No match found for token: /%s/\n", value);
                return 1;
        }
    }


    return 0;
}

/* argument parsing */
static void
parse_args(int argc, char **argv)
{
    char *tmp;
    int c;
    int index = 0;
    int error = 0;
    int quit = 0;

    static struct option lopt[] = {
        {"order",       required_argument,  0,  'o'},
        {"sort",        required_argument,  0,  's'},
        {"full-path",   no_argument,        0,  'f'},
        {"help",        no_argument,        0,  'h'},
        {"extension",   required_argument,  0,  'e'},
        {"blacklist",   required_argument,  0,  'b'},
        {"version",     no_argument,        0,  'v'},
        {"whitelist",   required_argument,  0,  'w'},
        {"",            0,                  0,  0 }
    };

    while ((c = getopt_long(argc, argv, "hfvo:s:e:b:w:", lopt, &index)) != -1)
    {
        switch (c)
        {
            case 'b':
                if(parse_extensions(optarg, &config.black) != 0)
                    error = 1;
                break;
            case 'e':
                if(parse_extensions(optarg, &config.exts) != 0)
                    error = 1;
                break;
            case 'f':
                config.full = 1;
                break;
            case 'h':
                display_help(argv[0]);
                quit = 1;
                break;
            case 'o':
            case 's':
                if(config.sorting ||
                   parse_sorting(optarg, &config.sorting) != 0)
                    error = 1;
                break;
            case 'v':
                display_version();
                quit = 1;
                break;
            case 'w':
                if(parse_extensions(optarg, &config.white) != 0)
                    error = 1;
                break;
            case '?':
            default:
                error = 1;
                printf("Unknown argument \"%s\"\n", argv[optind]);
        }
    }

    if(!quit)
    {
        if(optind < argc)
        {
            if((config.folder = realpath(argv[optind], NULL)) == NULL)
            {
                if(!error)
                {
                    printf("Folder \"%s\" does not exists\n", argv[optind]);
                    quit = 1;
                }
                error = 1;
            }
        }
        else
            error = 1;
    }

    if(!config.folder && !quit)
    {
        error = 1;
        display_help(argv[0]);
    }

    if(error)
        quit = 1;

    if(quit)
    {
        config_clean();
        exit(error);
    }

    if(!config.sorting)
    {
        tmp = strdup(DEFAULT_SORTING);
        parse_sorting(tmp, &config.sorting);
        free(tmp);
    }
}

static int
check_extension(const char *filename, ext_t allowed, int defaultval)
{
    ext_t tmp = allowed;
    size_t len = strlen(filename);
    size_t i;

    if(!tmp)
        return defaultval;

    while(tmp)
    {
        i = 1;
        while(i <= tmp->len && i <= len &&
                tmp->ext[tmp->len - i] == filename[len - i])
            ++i;

        if(i == tmp->len+1 && filename[len - i] == '.')
            return 1;

        tmp = tmp->next;
    }

    return 0;
}

static int
check_whitelist(const char *filename, ext_t allowed)
{
    while(allowed)
    {
        if(strcmp(filename, allowed->ext) == 0)
            return 1;
        allowed = allowed->next;
    }

    return 0;
}

static int
filter(const struct dirent *d)
{
    struct stat st;
    if(strcmp(d->d_name, ".") == 0 ||
       strcmp(d->d_name, "..") == 0)
        return 0;
    stat(d->d_name, &st);
    if(S_ISDIR(st.st_mode))
        return 1;
    if(check_whitelist(d->d_name, config.white) == 1)
        return 1;
    if(check_extension(d->d_name, config.black, 0) == 1)
        return 0;
    if(check_extension(d->d_name, config.exts, 1) == 1)
        return 1;
    return 0;
}

static int
ordersort(const struct dirent **d, const struct dirent **e)
{
    int i = 0;
    char a, b;

    while(42)
    {
        a = (*d)->d_name[i];
        b = (*e)->d_name[i];

        if(a >= 'A' || a <= 'Z')
            a += 32;
        if(b >= 'A' || b <= 'Z')
            b += 32;

        if(a < b)
            return 1;
        else if(b < a)
            return -1;
        else
            i++;
    }
    return 0;
}

#ifdef USE_MMAP
static void
count_in_file(const char *filename, fileinfos_t infos)
{
    struct stat st;
    int fd = open(filename, O_RDONLY);
    int space = -1;
    char *mem;
    char *c;

    infos->lines = 0;
    infos->chars = 0;
    infos->words = 0;

    if(fd == -1 || stat(filename, &st) < 0)
        return;

    mem = (char *)mmap(0, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    close(fd);

    c = (char *)mem;


    while(c - mem < st.st_size)
    {
        infos->chars++;
        if(*c == ' ' || *c == '\n')
        {
            if(space == 0)
            {
                infos->words++;
                space = 1;
            }
        }
        else
            space = 0;
        if(*c == '\n')
        {
            infos->lines++;
            space = -1;
        }
        c++;
    }

    munmap(mem, st.st_size);
}
#else
static void
count_in_file(const char *filename, fileinfos_t infos)
{
    FILE *f = fopen(filename, "r");
    int c;
    int space = -1;
    infos->lines = 0;
    infos->chars = 0;
    infos->words = 0;
    if(!f)
        return;
    while((c = fgetc(f)) != EOF)
    {
        infos->chars++;
        if(c == ' ' || c == '\n')
        {
            if(space == 0)
            {
                infos->words++;
                space = 1;
            }
        }
        else
            space = 0;
        if(c == '\n')
        {
            infos->lines++;
            space = -1;
        }
    }
    fclose(f);
}
#endif /* USE_MMAP */

static char *
copy_folder_name()
{
    size_t i = 0;
    char *path = get_current_dir_name();
    if(!config.full)
    {
        while(config.folder[i] == path[i] && path[i])
            ++i;
        strcpy(path, path+i);
    }
    return path;
}

static void
analyse(const char *filename)
{
    fileinfos_t infos = malloc(sizeof(fileinfos_s));
    count_in_file(filename, infos);
    infos->path = copy_folder_name();
    infos->name = strdup(filename);
    list_add(infos);
}

/*
** when a link is found check if a folder or a file and if a folder limit
** infinite recurse
*/
static void
process_link(const char *linkname)
{
    printf("link: %s\n", linkname);
}

/* count files and lines in a dir */
static void
count_dir(const char *dirname)
{
    struct dirent **ent;
    struct stat st;
    int n, i;

    if(chdir(dirname) != 0)
        return;

    n = scandir(".", &ent, filter, ordersort);
    /*n = scandir(dirname, &ent, filter, ordersort);*/
    /* errno test */

    if(n < 0)
    {
        /* error reading directory */
    }
    else
    {
        /*chdir(dirname);*/
        for(i = 0; i < n; ++i)
        {
            stat(ent[i]->d_name, &st);
            if(S_ISDIR(st.st_mode))
                count_dir(ent[i]->d_name);
            else if(S_ISREG(st.st_mode))
                analyse(ent[i]->d_name);
            else if(S_ISLNK(st.st_mode))
                process_link(ent[i]->d_name);
            // links
            free(ent[i]);
        }
        free(ent);
    }
    chdir("..");
}

static size_t
numlen(unsigned long long num)
{
    int i = 1;
    while((num /= 10) != 0)
        i++;
    return i;
}

static char *
str_repeat(char c, size_t l)
{
    char *s = malloc(l * sizeof(char) + 1);
    char *tmp = s;

    while(l--)
    {
        *tmp = c;
        ++tmp;
    }
    *tmp = '\0';
    return s;
}

static int
sorting_func_folder(fileinfos_t one, fileinfos_t two, int order)
{
    if(order == ORDER_ASC)
        return strcasecmp(one->path, two->path) > 0;
    return strcasecmp(one->path, two->path) < 0;
}

static int
sorting_func_file(fileinfos_t one, fileinfos_t two, int order)
{
    if(order == ORDER_ASC)
        return strcasecmp(one->name, two->name) > 0;
    return strcasecmp(one->name, two->name) < 0;
}

static int
sorting_func_lines(fileinfos_t one, fileinfos_t two, int order)
{
    if(order == ORDER_ASC)
        return one->lines > two->lines;
    return one->lines < two->lines;
}

static int
sorting_func_words(fileinfos_t one, fileinfos_t two, int order)
{
    if(order == ORDER_ASC)
        return one->words > two->words;
    return one->words < two->words;
}

static int
sorting_func_chars(fileinfos_t one, fileinfos_t two, int order)
{
    if(order == ORDER_ASC)
        return one->chars > two->chars;
    return one->chars < two->chars;
}

static void
sorting_select(int (*func)(fileinfos_t i1, fileinfos_t i2, int order), int ord)
{
    list_t cur = filelist, cmp, tmp;
    filelist = NULL;
    lastitem = NULL;

    while(cur)
    {
        cmp = cur;
        tmp = cur->next;

        while(tmp)
        {
            if(func(cmp->item, tmp->item, ord))
                cmp = tmp;
            tmp = tmp->next;
        }

        // restructuring old list
        if(cmp->next)
            cmp->next->prev = cmp->prev;
        if(cmp->prev)
            cmp->prev->next = cmp->next;

        // inserting in the new list
        if(!filelist)
            filelist = cmp;
        if(lastitem)
            lastitem->next = cmp;
        cmp->prev = lastitem;
        lastitem = cmp;

        // moving in the old list if necessary
        if(cur == cmp)
            cur = cur->next;
    }
}

static void
sorting()
{
    sort_t sort = config.sorting;
    while(sort)
    {
        switch(sort->sort)
        {
            case SORT_FOLDER:
                sorting_select(&sorting_func_folder, sort->order);
                break;
            case SORT_FILE:
                sorting_select(&sorting_func_file, sort->order);
                break;
            case SORT_LINES:
                sorting_select(&sorting_func_lines, sort->order);
                break;
            case SORT_WORDS:
                sorting_select(&sorting_func_words, sort->order);
                break;
            case SORT_CHARS:
                sorting_select(&sorting_func_chars, sort->order);
                break;
        }
        sort = sort->next;
    }
}

static void
gen_format_str(char **format, char **separator, char **head)
{
    list_t tmp = filelist;
    size_t *array = malloc(6 * sizeof(size_t));
    char *t = NULL, *c;
    size_t size;
    int i;

    array[0] = 0;
    array[1] = 4;
    array[2] = 4;
    array[3] = 5;
    array[4] = 5;
    array[5] = 5;

    while(tmp)
    {
        array[0]++;
        array[1] = MAX(array[1], strlen(tmp->item->path));
        array[2] = MAX(array[2], strlen(tmp->item->name));
        array[3] = MAX(array[3], numlen(tmp->item->lines));
        array[4] = MAX(array[4], numlen(tmp->item->words));
        array[5] = MAX(array[5], numlen(tmp->item->chars));
        tmp = tmp->next;
    }

    array[0] = MAX(2, numlen(array[0]));

    size = 40 + array[0] + numlen(array[1]) + numlen(array[2])
              + array[3] + array[4] + array[5];
    (*format) = malloc(size * sizeof(char));

    size = 40 + array[0] + array[1] + array[2] + array[3] + array[4] + array[5];
    (*head) = malloc(size * sizeof(char));
    (*separator) = malloc(size * sizeof(char));

    sprintf(*format, "| %%-%lud | %%-%lus | %%-%lus | %%-%lulu | %%-%lulu | %%-%lulu |\n",
            array[0], array[1], array[2], array[3], array[4], array[5]);

    c = *separator;
    for(i = 0; i < 6; i++)
    {
        t = str_repeat('-', array[i]);
        strcpy(c, "+-");
        c += 2;
        strcpy(c, t);
        c += array[i];
        strcpy(c, "-");
        c += 1;
        free(t);
    }
    strcpy(c, "+");

    sprintf(*head, "| %-*s | %-*s | %-*s | %-*s | %-*s | %-*s |",
            (int)array[0], "no",    (int)array[1], "path",
            (int)array[2], "name",  (int)array[3], "lines",
            (int)array[4], "words", (int)array[5], "chars");
    free(array);
}

static void
print_total(uint64_t lines, uint64_t words, uint64_t chars, int files)
{
    if(files == 0)
        files = 1;

    printf("+----------+------------+------------+------------+\n");
    printf("| Total    | %-10lu | %-10lu | %-10lu |\n",
            lines, words, chars);
    printf("| Average  | %-10lu | %-10lu | %-10lu |\n",
            lines/files, words/files, chars/files);
    printf("+----------+------------+------------+------------+\n");
}

static void
print_result()
{
    list_t tmp = filelist;
    uint32_t i = 0;
    uint64_t lines = 0, words = 0, chars = 0;
    char *format = NULL, *separator = NULL, *head = NULL;
    char *tmppath = NULL;
    gen_format_str(&format, &separator, &head);

    puts(separator);
    puts(head);
    puts(separator);

    while(tmp)
    {
        i++;
        lines += tmp->item->lines;
        chars += tmp->item->chars;
        words += tmp->item->words;

        tmppath = tmp->item->path;
        if(!tmppath[0])
            tmppath = "/";

        printf(format, i, tmppath, tmp->item->name,
                tmp->item->lines, tmp->item->words, tmp->item->chars);
        tmp = tmp->next;
    }

    puts(separator);
    putchar('\n');
    print_total(lines, words, chars, i);

    free(format);
    free(separator);
    free(head);
}

/* entry point */
int
main(int argc, char **argv)
{
    parse_args(argc, argv);
    count_dir(config.folder);
    sorting();
    print_result();
    list_clean();
    config_clean();
    return EXIT_SUCCESS;
}

// -blacklist dirs
//
// -directory reading error
// -count_in_dir : error support for files
// -use an append_error(va_args) function
//
// -support link
// -limit recurse
//
// -default filter are dirty (strdup)
//
// -function pointer on the comparison function instead of switch case
//  and also
// -parsing LS_COLORS
// -fancy and plaintext mode
//
