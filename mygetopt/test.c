#include <stdio.h>
#include <stdlib.h>
#include "opt.h"

// the OPT_REQUIRED could catch anything regardless of what it is

const argopt ops[] = {
    { "version", 0, 'V', OPT_NONE },
    { "verbose", 0, 'v', OPT_NONE },
    { "warning", 0, 'W', OPT_NONE },
    { "help",    1, 'h', OPT_MBGLUED | OPT_OPTIONAL },
    { "name",    1, 'n', OPT_GLUED | OPT_MALLOC },
    { "full",    2, 'f', OPT_REQUIRED },
    { "anon",    2, 'a', OPT_OPTIONAL | OPT_MALLOC },
    { 0,         0,   0, 0 }
};

const argmeta opsmeta[] = {
    { "Display the version of the program", "...", NULL },
    { "Enable the verbosity of the program", "...", NULL },
    { "Report warnings", "...", NULL },
    { "Display this help",
      "this option can take an extra argument to get the help of a command",
      "[command]"
    },
    { "Get the name of a person", NULL, "name" },
    { "Get the full name of a person", NULL, "firstname lastname" },
    { "Become anonymous", NULL, "firstname lastname" }
};


int main(int argc, const char *argv[])
{
    char *tmp = NULL, *tmp2 = NULL;
    int res, opt;
    arginf inf;

    optinit(&inf, argc, argv, ops, opsmeta);

    while((opt = getopt(&inf)) != OPT_END)
    {
        /*printf("arg %-10s doing %c having %d params (%08X) | next is %d\n", 
                argv[inf.index], ops[inf.pos].value, inf.params, inf.flags, inf.next);*/
        switch(opt)
        {
            case 'a':
                res = getparams(&inf, &tmp, &tmp2);
                if(res < 2)
                {
                    printf("apparently you want to be anon\n");
                    if(res == 1)
                        free(tmp);
                }
                else
                {
                    printf("ahah u noob ! %s %s\n", tmp, tmp2);
                    free(tmp);
                    free(tmp2);
                }
                break;
            case 'V':
                puts("version 0.0.0.0.0.1 ?");
                return EXIT_SUCCESS;

            case 'v':
                puts("verbose mode on");
                break;

            case 'h':
                res = getparams(&inf, &tmp);
                if(res == 0)
                    usage(&inf);
                else
                    usage_cmd(&inf, tmp);
                break;

            case 'n':
                res = getparams(&inf, &tmp);
                if(res != 1)
                    printf("OMFG WTF BOOOOOOOOOOOOM !!!\n");
                else
                    printf("Hello %s, Are u doing well ?\n", tmp);
                free(tmp);
                break;

            case 'f':
                res = getparams(&inf, &tmp, &tmp2);
                if(res != 2)
                    printf("U fking anonymous !\n");
                else
                    printf("Nice to meet u %s %s\n", tmp, tmp2);
                break;

            case 'W':
                puts("Warning are now reported");
                break;

            case OPT_UKN:
                printf("Unknown argument %s\n", argv[inf.index]);
                break;

            case OPT_PAR:
                printf("Param error concerning %s\n", argv[inf.index]);
                break;

            case OPT_ERR:
                printf("An error occured at %s\n", argv[inf.index]);
                break;
        }
    }

    return EXIT_SUCCESS;
}
