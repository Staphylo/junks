#ifndef OPT_H
#define OPT_H

/*
** A new option parsing lib
** with much more options
** feel free to report bugs
*/

// TODO: callbacks for options

/* when the arguments are all parsed */
#define OPT_END      -1
/* when an unknown argument has been found */
#define OPT_UKN      -2
/* when a parameter is missing */
#define OPT_PAR      -3
/* something bad happened man... just kidding, a lost value was found */
#define OPT_ERR      -42

/******************************************************************************
** Flags
******************************************************************************/

/* not necessary but for lisibility */
#define OPT_NOTGLUED 0x00
/* the parameter is sticked to the option */
#define OPT_GLUED    0x01
/* the parameter can be or not sticked to the option*/
#define OPT_MBGLUED  0x02
/* if there is short options attached */
#define OPT_MULTIPLE 0x04

/* no special options */
#define OPT_NONE     0x00
/* will malloc the value rather than doing a reference on the array */
#define OPT_MALLOC   0x20
/* an argument can be given to this option or not */
#define OPT_OPTIONAL 0x40
/* an argument must be given when this option is set */
#define OPT_REQUIRED 0x80


/******************************************************************************
** typedefs
******************************************************************************/


typedef enum callback_res_e callback_res_e;
typedef callback_res_e (*opt_callback_f)(int, const char **);
typedef struct argopt argopt;
typedef struct arginf arginf;
typedef struct argmeta argmeta;


/******************************************************************************
** strucure declaration
******************************************************************************/

enum callback_res_e
{
    CALLBACK_SUCCESS,
    CALLBACK_ERROR,
    CALLBACK_FATAL
};

struct argmeta
{
    /* comment */
    const char *comment;
    /* help */
    const char *help;
    /* args */
    const char *args;
};

struct argopt
{
    /* name of the option */
    const char *name;
    /* number of parameters */
    int params;
    /* short name and/or value to be returned */
    int value;
    /* flags for the current option */
    int flags;
};

struct arginf
{
    /* current index in argv */
    int index;
    /* next index targetted */
    int next;
    /* number of parameters to read */
    int params;
    /* position of the option in the argopt array */
    int pos;
    /* is the param glued */
    int flags;
    /* argc */
    int argc;
    /* argv */
    const char **argv;
    /* argopt */
    const argopt *ops;
    /* argmeta */
    const argmeta *meta;
};


/******************************************************************************
** prototypes
******************************************************************************/

/*
** init the arginf struct for further use
** inf  : struct to initialize
** argc : argument count
** argv : argument vector
** ops  : argopt struct
** meta : optional argmeta struct
*/
void optinit(arginf *inf, int argc, const char *argv[], const argopt ops[],
             const argmeta meta[]);

/*
** display the usage of the program
** inf : arginf struct
*/
void usage(const arginf *inf);

/*
** display the usage of a command mostly usefull with a argmeta struct
** inf : arginf struct
** cmd : the command to lookup
*/
void usage_cmd(const arginf *inf, const char *cmd);

/*
** getopt_long like function
** inf  : structure arginf (initialized with optinit)
** return a value from a corresponding entry of ops or an error (see above)
*/
int getopt(arginf *inf);

/*
** new function
** inf  : structure arginf used by getopt (see above)
** ...  : char ** parameters to get the args
** return the number of parameters cought
*/
int getparams(arginf *inf, ...);

#endif /* !OPT_H */
