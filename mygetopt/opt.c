#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include "opt.h"

#define BINCHECK(val, flag)  (val & flag) == flag
#define ISOPTIONAL(val)      BINCHECK(val, OPT_OPTIONAL)
#define ISGLUED(val)         BINCHECK(val, OPT_GLUED)
#define ISMBGLUED(val)       BINCHECK(val, OPT_MBGLUED)
#define ISMULTIPLE(val)      BINCHECK(val, OPT_MULTIPLE)
#define DOMALLOC(val)        BINCHECK(val, OPT_MALLOC)

#define RESETBUTLET(val, flags) val &= (~val) | flags

#define SETFLAGS(val, flags)   val |= flags
#define UNSETFLAGS(val, flags) val &= ~flags

#define OPT_SHORT 0x00
#define OPT_LONG  0x01

/*
** compare two a options.name field with an argument freed of it's --
** return the 0 if they are the same and a number corresponding of the
** comparison done
*/
static int namecmp(const char *opname, const char *arg)
{
    const char *o = opname;
    const char *a = arg;
    int i = 0;

    while((*a != '\0' && *a != '=') || (*o != '\0'))
    {
        i++;
        if(*o == *a)
        {
            o++;
            a++;
            continue;
        }

        return i;
    }

    return (*o == '\0' && (*a == '\0' || *a == '=')) ? 0 : i;
}

/*
** compare the argument name with each name contained in the options structure
** until it find a corespondance.
** if no one is exactly the same it check if it is a partial argument an that
** there is no collisions.
** the function return the index of the corresponding options in the array
*/
static int getopfromname(const argopt *ops, const char *name, int offset)
{
    int i = 0;
    int res;
    int max = 0;
    int ind = 0;

    while(ops[i].name != NULL)
    {
        if((res = namecmp(ops[i].name, name + offset)) == 0)
            return i;
        else if(res >= max)
        {
            if(res == max)
                ind = -1;
            else if(res > max)
            {
                ind = i;
                max = res;
            }
        }
        i++;
    }
    return ind;
}

/*
** return the index of the option structure contained in the array given
** which have a field value corresponding with the calue given
*/
static int getopfromvalue(const char value, const argopt *ops)
{
    int i = 0;
    while(ops[i].name != NULL)
    {
        if(ops[i].value == (int)value)
                return i;
        i++;
    }

    return -1;
}

/*
** it check if an argument is an option and return the index
*/
static int isoption(const argopt *ops, const char *arg)
{
    int i = -1, res;

    if(arg == NULL)
        return 0;

    if(arg[0] != '\0' && arg[1] != '\0' && arg[0] == '-')
    {
        if(arg[1] == '-')
        {
            while(ops[++i].name != NULL)
                if((res = getopfromname(ops, arg, 2)) != -1)
                    return res;
        }
        else
        {
            while(ops[++i].name != NULL)
                if((res = getopfromvalue(arg[1], ops)) != -1)
                    return res;
        }
    }

    return -1;
}

/*
** check if there is a glued parameter
*/
static int isgluedparam(const char *arg)
{
    const char *c = arg;
    if(arg[1] == '-')
    {
        while (*(c++) != '\0')
            if(*c == '=')
                return 1;
    }
    else
        return !(arg[2] == '\0');

    return 0;
}

int getopt(arginf *inf)
{
    size_t len;
    argopt op;
    const char *arg;
    int tmp;
    int type;

    int argc = inf->argc;
    const char **argv = inf->argv;
    const argopt *ops = inf->ops;

    inf->next = inf->index + ((ISGLUED(inf->flags) ||
                ISMULTIPLE(inf->flags)) ? 0 : inf->params) +
                ((ISMULTIPLE(inf->flags)) ? 0 : 1);
    inf->index = inf->next;
    inf->next = inf->index + 1;

    arg = argv[inf->index];

    if(inf->index < argc)
    {
        len = strlen(arg);
        if(len >= 2 && arg[0] == '-')
        {
            /* what kind of option? */
            type = (arg[1] == '-') ? OPT_LONG : OPT_SHORT;

            /* retrieving corresponding argopt struct */
            if(type == OPT_SHORT)
            {
                if(ISMULTIPLE(inf->flags))
                    inf->pos = getopfromvalue(arg[-inf->params+1], ops);
                else
                    inf->pos = getopfromvalue(arg[1], ops);
            }
            else
                inf->pos = getopfromname(ops, arg, 2);

            /* checking if it's a known arg */
            if(inf->pos != -1)
            {
                op = ops[inf->pos];
                RESETBUTLET(inf->flags, OPT_MULTIPLE);
                SETFLAGS(inf->flags,
                        ((DOMALLOC(op.flags)) ? OPT_MALLOC : OPT_NONE));

                if(op.params == 0 || ISMULTIPLE(inf->flags))
                {
                    /* no parameters are required */
                    if(len > 2 && type == OPT_SHORT)
                    {
                        if(ISMULTIPLE(inf->flags))
                        {
                            if((size_t)(-inf->params + 2) == len)
                            {
                                UNSETFLAGS(inf->flags, OPT_MULTIPLE);
                                inf->params = 0;
                            }
                            else
                                inf->params--;
                        }
                        else
                        {
                            /* bad to use this variable here but need an int */
                            type = 2;
                            while(arg[type] != '\0')
                            {
                                tmp = 0;
                                while(ops[tmp].name != NULL &&
                                    ops[tmp].value != arg[type])
                                    tmp++;

                                if(ops[tmp].value == 0 ||
                                        (ops[tmp].params != 0 &&
                                        (ops[tmp].params == 1 &&
                                        !ISOPTIONAL(ops[tmp].flags))))
                                    return OPT_ERR;

                                type++;
                            }

                            SETFLAGS(inf->flags, OPT_MULTIPLE);
                            inf->params = -1;
                        }

                        return op.value;
                    }
                    else
                    {
                        inf->params = 0;
                        return op.value;
                    }
                }
                else if(ISOPTIONAL(op.flags)|| ((ISGLUED(op.flags) ||
                          ISMBGLUED(op.flags)) && op.params == 1)  ||
                        op.params <= argc - inf->index - 1)
                {
                    /* if the parameters are optional or if there is enough
                    ** place for them in the array or if the parameter is
                    ** glued to the value
                    */
                    if(op.params == 1)
                    {
                        inf->params = 1;
                        if(ISGLUED(op.flags))
                        {
                            SETFLAGS(inf->flags, OPT_GLUED);
                            if(isgluedparam(arg) == 1)
                            {
                                return op.value;
                            }
                            else if(ISOPTIONAL(op.flags))
                            {
                                //SETFLAGS(inf->flags, OPT_GLUED);
                                inf->params = 0;
                                return op.value;
                            }
                            else
                                return OPT_PAR;
                        }
                        else if(ISMBGLUED(op.flags))
                        {
                            if(isgluedparam(arg) == 1)
                            {
                                SETFLAGS(inf->flags, OPT_GLUED) ;
                                return op.value;
                            }
                            else if(isoption(ops, argv[inf->index + 1]) == -1)
                            {
                                UNSETFLAGS(inf->flags, OPT_GLUED);
                                return op.value;
                            }
                            else if(ISOPTIONAL(op.flags))
                            {
                                inf->params = 0;
                                return op.value;
                            }
                            else
                                return OPT_PAR;
                        }
                        else
                        {
                            if(isoption(ops, argv[inf->index+1]) == -1)
                            {
                                return op.value;
                            }
                            else if(ISOPTIONAL(op.flags))
                            {
                                inf->params = 0;
                                return op.value;
                            }
                            else
                                return OPT_PAR;
                        }
                    }
                    else
                    {
                        /* more than one parameter */
                        if(ISOPTIONAL(op.flags))
                        {
                            for(tmp = 1; tmp <= op.params; ++tmp)
                            {
                                if(inf->index + tmp >= argc ||
                                    isoption(ops, argv[inf->index + tmp]) != -1)
                                    break;
                            }
                            inf->params = tmp-1;
                        }
                        else
                            inf->params = op.params;

                        return op.value;
                    }
                }
                else
                {
                    /* parmeters aren't were they are expected to be */
                    return OPT_PAR;
                }
            }
            else
            {
                /* it's an unknown arg */
                return OPT_UKN;
            }
        }
        else
        {
            /* not an argument */
            return OPT_ERR;
        }
    }
    else
    {
        /* end of arg list */
        return OPT_END;
    }
}


/*
** it puts in the arguments the values of the parameters after the argument
** and return the number of parameters cought
*/
int getparams(arginf *inf, ...)
{
    int i = 0;
    char **tmp;
    char *c;
    va_list list;
    char const **argv = inf->argv;

    if(inf->params <= 0)
        return 0;

    va_start(list, inf);

    if(ISGLUED(inf->flags))
    {
        c = (char *)argv[inf->index];
        if(c[1] == '-')
            while(*(c++) != '=');
        else
            c += 2;

        if(c == '\0')
            return 0;

        tmp = va_arg(list, char **);
        if(DOMALLOC(inf->flags))
        {
            (*tmp) = malloc(strlen(c) * sizeof(char));
            strcpy((*tmp), c);
        }
        else
            (*tmp) = c;

        va_end(list);
        return 1;
    }

    do
    {
        i++;
        tmp = va_arg(list, char **);
        if(DOMALLOC(inf->flags))
        {
            (*tmp) = malloc(strlen(argv[inf->index + i]) * sizeof(char));
            strcpy((*tmp), argv[inf->index + i]);
        }
        else
            (*tmp) = (char *)argv[inf->index + i];

    } while (i < inf->params);
    va_end(list);

    return i;
}

void optinit(arginf *inf, int argc, const char *argv[], const argopt ops[],
             const argmeta meta[])
{
    memset(inf, 0, sizeof (struct arginf));
    inf->argc = argc;
    inf->argv = argv;
    inf->ops = ops;
    inf->meta = meta;
}

void usage(const arginf *inf)
{
    int max = 0;
    const argopt *ops = inf->ops;
    const argmeta *meta = inf->meta;

    for (int i = 0; ops[i].name; ++i)
    {
        int len = sizeof(ops[i].name);
        if (len > max)
            max = len;
    }

    // TODO: generate list of arguments
    printf("usage: %s [OPTIONS]\n", inf->argv[0]);
    printf("\nOptions:\n\n");
    for (int i = 0; ops[i].name; ++i)
    {
        printf("   -%c --%-*s", ops[i].value, max, ops[i].name);
        if (meta)
            printf("   %s\n", meta[i].comment);
        else
            putchar('\n');
    }
    printf("\n");
}


void usage_cmd(const arginf *inf, const char *cmd)
{
    int idx = getopfromname(inf->ops, cmd, 0);

    // TODO: print all possible usage -c arg -c [args] --cmd=args --cmd [args]
    if (idx < 0)
        printf("Unknown command %s\n", cmd);
    else if (!inf->meta)
    {
        const argopt *op = inf->ops + idx;

        printf("Command: %s\n", op->name);
        printf("Syntax: -%c --%s\n", op->value, op->name);
    }
    else
    {
        const argopt *op = inf->ops + idx;
        const argmeta *me = inf->meta + idx;

        printf("Command: %s\n", op->name);
        printf("Syntax: -%c --%s ", op->value, op->name);
        if (me->args)
            puts(me->args);
        else
            putchar('\n');
        if (me->comment)
            printf("Comment: %s\n", me->comment);
        if (me->help)
            printf("Help:\n\n%s\n\n", me->help);
        else
            printf("Help: none available\n");
    }
}
