#ifndef ARGPARSE_H_
# define ARGPARSE_H_

# include <stdbool.h>

# include "vector.h"
# include "hashtbl.h"

// forward declaration
struct argparse;

/**
 ** @brief Option description structure
 ** This also represents the parameters sent to argparse_option
 */
struct argparse_opt {
    /** Short option (starting with -) */
    char shopt;
    /** Long option (starting with --) */
    const char *longopt;
    /** Help message for this option */
    const char *help;
    /** Number of parameters */
    int params;
    /** Number of required parameters */
    int required;
    // add an option descriptor
};

VECTOR_DECLARE(struct argparse_opt*, option_vect)

/**
 ** @brief Command description structur
 ** This structure also represents the parameters sent to argparse_command
 */
struct argparse_cmd {
    /** Name of the command to describe */
    const char *name;
    /** Help message for this command */
    const char *help;
    /** Vector of options that goes with the command (internal stuff) */
    option_vect_t options;
};

VECTOR_DECLARE(struct argparse_cmd*, command_vect)

/**
 ** @brief Type of the data stored
 */
typedef enum {
    ARGPARSE_TYPE_NONE,
    ARGPARSE_TYPE_ARRAY,
    ARGPARSE_TYPE_STRING,
    ARGPARSE_TYPE_INT,
} argparse_val_e;

/**
 ** @brief Argparse value which is a pair of a type and a data
 */
struct argparse_val {
    /** Type of the value stored in data */
    argparse_val_e type;
    /** Pointer to the data (which needs to be casted using type) */
    void *data;
};

HASHTBL_DECLARE(const char *, struct argparse_val *, argparse_data)

VECTOR_DECLARE(struct argparse_val *, argparse_val_vect)

/**
 ** @brief This structure is used to configure the behaviour of the parser.
 ** Programs tends to have very different option handling. The behaviours of
 ** all option parser behaviour can't be combined as they conflit cometimes.
 ** To make it possible to do anything, this structure exists
 */
typedef struct argparse_behaviour {
    /** Allow the parameter of a short option to be glued
     ** e.g: gcc -Wall -c -oargparse.o argparse.c
     */
    bool shopt_glued; // ls -ofile
    /** Option can be glued together
     ** e.g: tar -xvJf argparse.tar.xz
     */
    bool shopt_multiple; // tar -xvf file
    /** Allow long option parameter to be glued to the option using =
     ** e.g: airodump-ng --encrypt=WEP
     */
    bool longopt_glued; // --output=file
    /** Consider coma words as parameters
     */
    bool longopt_multiple; // --output=file1,file2,file3
    /** Allow long options to be preceeded by only one dash
     ** e.g: pdflatex -halt-on-error
     */
    bool shopt_long; // -help
    /** Generate the help message for you ;) */
    bool default_help; // git --help
    /** Generate a per command help
     ** e.g: git pull --help , git --help pull
     */
    bool command_help; // git push --help or git --help push
    /** Stop scanning for options after -- */
    bool optscan_stop; // --
    /** Display the version of the program for you when given a function ptr */
    void (*version)(struct argparse *argp);
} argparse_behaviour_s;

/**
 ** @brief 
 */
struct argparse {
    /** */
    const char *prgname;
    /** */
    argparse_behaviour_s mood;
    /** */
    command_vect_t commands; // if set it uses sub commands
    /** */
    option_vect_t options; // if set have some options without commands
    /** */
    argparse_data_t data;
};

struct argparse_cmd *argparse_command_create(struct argparse *argp, struct argparse_cmd params);

/**
 ** @brief Create a command argument
 **
 ** @param .name Name of the command
 ** @param .help Help associated to the command
 **
 ** @return The id of the newly created command
 */
#define argparse_command(Argp, ...) \
    argparse_command_create(Argp, (struct argparse_cmd){ \
                                  .help = "",\
                                  .options = NULL,\
                                  __VA_ARGS__})


struct argparse_opt *argparse_option_create(option_vect_t opts, struct argparse_opt in);

/**
 ** @brief Create a option argument
 **
 ** @param .shopt Short option associated
 ** @param .longopt Long option associated
 ** @param .help Help associated to the option
 **
 ** @return The id of the newly created option
 */
#define argparse_option(Argp, ...) \
    argparse_option_create(Argp->options, (struct argparse_opt){ \
                                 .help = "",\
                                 .shopt = 0, \
                                 .params = 0, \
                                 .required = 0, \
                                 __VA_ARGS__})


/**
 ** @brief Create a new argparse value
 **
 ** @param Type Type of the variable (argparse_val_e)
 ** @param Data The data to store in the value
 **
 ** @return the newly created argparse value
 */
#define argparse_val(Type, Data) \
    argparse_val_create(ARGPARSE_TYPE_ ## Type, (void*)Data);

/**
 ** @brief Free argparse structure
 **
 ** @param argp the argparse structure
 */
void argparse_free(struct argparse *argp);

/**
 ** @brief Create an argparse structure
 **
 ** @param beha behaviour structure for the parser
 **
 ** @return The newly created structure or NULL
 */
struct argparse *argparse_create(struct argparse_behaviour beha);

/**
 ** @brief Create a new argparse parser
 **
 ** @param ... Fields of the structure argparse_behaviour
 **
 ** @return The newly created argparse parser
 */
#define argparse(...) \
    argparse_create((struct argparse_behaviour){ \
                    .shopt_glued = false, \
                    .shopt_multiple = true, \
                    .shopt_long = false, \
                    .longopt_glued = true, \
                    .longopt_multiple = false, \
                    .default_help = true, \
                    .command_help = true, \
                    .version = NULL, \
                    ##__VA_ARGS__})


/**
 ** @brief This function parse the arguments corresponding to the argp structure
 **
 ** @param argp initialized argparse structure
 ** @param argc argument count
 ** @param argv argument vector
 **
 ** @return 1 on sucess, 0 on normal exit, -1 on error
 */
int argparse_parse(struct argparse* argp, int argc, const char **argv);

void argparse_help(struct argparse* argp, struct argparse_cmd *cmd);

bool argparse_data_exists(struct argparse *argp, const char *key);

void argparse_data_dump(struct argparse* argp);

const char *argparse_data_array_at(struct argparse *argp, const char *key, size_t idx);


#endif
