#define _XOPEN_SOURCE 700
#define _GNU_SOURCE
#include <string.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "argparse.h"



void argparse_data_free(argparse_data_item_t value)
{
    (void)value;
    printf("freeing value\n");
}

int argparse_data_keycmp(const char *s1, const char *s2)
{
    return strcmp(s1, s2);
}

int argparse_data_keyhash(const char *key)
{
    int hash = 5381;
    for (; *key; key++)
        hash = ((hash << 5) + hash) + *key;
    return hash;
}

void argparse_val_dump(struct argparse_val *val)
{
    switch (val->type) {
        case ARGPARSE_TYPE_ARRAY:
            printf("[ ");
            struct argparse_val **it_val;
            argparse_val_vect_t vect = val->data;
            vector_foreach(vect, it_val) {
                argparse_val_dump(*it_val);
                printf(" ");
            }
            printf("]");
            break;
        case ARGPARSE_TYPE_INT:
            printf("%ld", (long int)val->data);
            break;
        case ARGPARSE_TYPE_STRING:
            printf("%s", (const char*)val->data);
            break;
        case ARGPARSE_TYPE_NONE:
            printf("(none)");
        default:
            printf("???");
    }
}

void argparse_data_dump(struct argparse *argp)
{
    argparse_data_t data = argp->data;
    hashtbl_foreach(data) {
        struct argparse_val *val = data->bucket[i]->it->data.value;
        const char *key = data->bucket[i]->it->data.key;
        printf("[~] %s = ", key);
        argparse_val_dump(val);
        printf("\n");
    }
}


// values
struct argparse_val *argparse_val_create(argparse_val_e type, void *data)
{
    struct argparse_val *value = malloc(sizeof(struct argparse_val));
    if (!value)
        return NULL;
    value->type = type;
    value->data = data;
    return value;
}

void argparse_val_free(struct argparse_val *value)
{
    free(value);
}

// Options

void argparse_option_free(struct argparse_opt *opt)
{
    if (!opt)
        return;

    free(opt);
}

struct argparse_opt *argparse_option_create(option_vect_t opts, struct argparse_opt in)
{
    struct argparse_opt *opt = malloc(sizeof (struct argparse_opt));
    if (!opt)
        return NULL;
    memcpy(opt, &in, sizeof (struct argparse_opt));
    vector_add(opts, opt);
    return opt;
}

// Commands

void argparse_command_free(struct argparse_cmd *cmd)
{
    if (!cmd)
        return;

    if (cmd->options)
        vector_free(cmd->options);
    free(cmd);
}

//struct argparse_cmd *argparse_command(struct argparse *argp, const char *name, const char *help)
struct argparse_cmd *argparse_command_create(struct argparse *argp, struct argparse_cmd params)
{
    struct argparse_cmd *cmd = calloc(1, sizeof (struct argparse_cmd));
    if (!cmd)
        return NULL;
    memcpy(cmd, &params, sizeof (struct argparse_cmd));
    vector_create(cmd->options, argparse_option_free);
    // TODO add option with --help
    vector_add(argp->commands, cmd);
    return cmd;
}

// ArgParse

void argparse_free(struct argparse *argp)
{
    vector_free(argp->commands);
    vector_free(argp->options);
    free(argp);
}


struct argparse *argparse_create(struct argparse_behaviour beha)
{
    //assert(beha.shopt_glued && beha.shopt_multiple);
    struct argparse *argp = calloc(1, sizeof (struct argparse));
    if (!argp)
        return NULL;
    memcpy(&argp->mood, &beha, sizeof (struct argparse_behaviour));
    hashtbl_init(argp->data, 23, argparse_data_keyhash, argparse_data_keycmp, NULL);
    vector_create(argp->commands, argparse_command_free);
    vector_create(argp->options, argparse_option_free);
    return argp;
}

struct argparse_cmd *argparse_command_find(const struct argparse *argp, const char *s)
{
    struct argparse_cmd *res;
    int i = 0;
    int matchs = 0;

    struct argparse_cmd **it_cmd;
    const char *cmd;
    vector_foreach(argp->commands, it_cmd) {
        cmd = (*it_cmd)->name;
        bool ok = true;
        for (int j = 0; s[j] && cmd[j]; ++j)
            if (cmd[j] != s[j])
                ok = false;
        if (ok) {
            res = *it_cmd;
            matchs++;
        }
        i++;
    }

    if (matchs == 1)
        return res;

    return NULL;
}

VECTOR_DECLARE(char *, argparse_str_vect);

struct argparse_ctx {
    int  argc;
    const char **argv;
    argparse_str_vect_t args;
    bool end;
    int index;
    int error;
    struct argparse_cmd *cmd;
    struct argparse *argp;
};

int argparse_parse_option(struct argparse_ctx *ctx, struct argparse_opt *opt)
{
    struct argparse_val *val;
    int required = opt->required;
    int i = 0;

    if (opt->params) {
        argparse_val_vect_t vect;
        vector_create(vect, argparse_val_free);

        for (; i < opt->params; ++i) {
            if (required) {
                required--;
                if (ctx->index + i + 1 >= ctx->argc) {
                    printf("[-] option: %s no more parameters\n", opt->longopt);
                    return -1;
                }

                printf("[+] option: %s -!> %s\n", opt->longopt,
                       ctx->argv[ctx->index + i + 1]);
            } else {
                if (ctx->index + i + 1 >= ctx->argc) {
                    printf("[-] option: %s no more parameters\n", opt->longopt);
                    break;
                }
                if (ctx->argv[ctx->index + i + 1][0] == '-') {
                    printf("[i] option: %s next parameter is an option\n", opt->longopt);
                    break;
                }

                printf("[+] option: %s --> %s\n", opt->longopt,
                       ctx->argv[ctx->index + i + 1]);
            }

            // fetch and store argument (convertion)
            // FIXME for multiple arguments an array is required
            val = argparse_val(STRING, ctx->argv[ctx->index + i + 1]);
            vector_add(vect, val);
        }
        val = argparse_val(ARRAY, vect);
        hashtbl_set(ctx->argp->data, opt->longopt, val);
    }
    else {
        val = argparse_val(STRING, NULL);
        hashtbl_set(ctx->argp->data, opt->longopt, val);
    }




    return i;
}

int argparse_parse_long(struct argparse_ctx *ctx)
{
    option_vect_t options;

    options = (ctx->cmd) ? ctx->cmd->options : ctx->argp->options;

    struct argparse_opt *opt, **opt_it;
    vector_foreach(options, opt_it) {
        opt = *opt_it;
        if (opt->longopt && !strcmp(opt->longopt, &ctx->argv[ctx->index][2])) {
            printf("[+] long option: %s\n", ctx->argv[ctx->index]);
            return argparse_parse_option(ctx, opt) + 1;
        }
    }

    return -1;
}

int argparse_parse_short(struct argparse_ctx *ctx)
{
    option_vect_t options;
    int ok = 0;
    int res = 0;

    options = (ctx->cmd) ? ctx->cmd->options : ctx->argp->options;

    struct argparse_opt *opt, **opt_it;
    for (int i = 1; ctx->argv[ctx->index][i]; ++i) {
        char c = ctx->argv[ctx->index][i];
        vector_foreach(options, opt_it) {
            opt = *opt_it;
            if (opt->shopt && opt->shopt == c) {
                if (opt->params && ctx->argv[ctx->index][i+1]) {
                    printf("[-] short option: -%c take parameters, so must the last", c);
                    return -1;
                }

                printf("[+] short option: -%c\n", c);
                int tmp = argparse_parse_option(ctx, opt);
                if (tmp < 0)
                    return -1;

                res += tmp;
                ok = 1;
            }
        }
    }

    if (ok)
        return res + 1;

    printf("[-] short option: %s not found\n", ctx->argv[ctx->index]);
    return -1;
}

argparse_str_vect_t argparse_parse_tokenize(struct argparse *argp, int argc,
                                            const char **argv)
{
    argparse_str_vect_t tokens = NULL;
    char *data;

    vector_create(tokens, (void (*)(char *))free);
    if (!tokens)
        return NULL;

    for (int i = 0; i < argc; ++i)
    {
        const char *str = argv[i];
        if (str[0] == '-') {
            if (str[1] == '-') {
                if (argp->mood.longopt_glued) {
                    const char *match = strchr(str, '=');
                    if (match) {
                        vector_add(tokens, strndup(str, match - str));
                        if (argp->mood.longopt_multiple) {
                            const char *prev = NULL;
                            do {
                                prev = match + 1;
                                match = strchr(prev, ',');
                                if (match)
                                    vector_add(tokens, strndup(prev, data-match));
                            } while (match);
                        } else if (*(match + 1)) {
                            vector_add(tokens, strdup(match + 1));
                        }
                    } else {
                        vector_add(tokens, strdup(str));
                    }
                } else {
                    vector_add(tokens, strdup(str));
                }

            } else {
                if (argp->mood.shopt_glued) {
                    asprintf(&data, "-%c", str[1]);
                    vector_add(tokens, data);
                    if (str[2])
                        vector_add(tokens, strdup(&str[2]));
                } else if (argp->mood.shopt_multiple) {
                    for (int j = 1; str[j]; ++j) {
                        asprintf(&data, "-%c", str[j]);
                        vector_add(tokens, data);
                    }
                } else if (argp->mood.shopt_long) {
                    // FIXME restart option parsing on long
                    asprintf(&data, "-%s", str);
                    vector_add(tokens, data);
                } else if (str[2]) {
                    printf("[!] No behaviour for multiple char shopt\n");
                } else {
                    vector_add(tokens, strdup(str));
                }
            }
        } else {
            vector_add(tokens, strdup(str));
        }
    }

    return tokens;
}

int argparse_parse(struct argparse* argp, int argc, const char **argv)
{
    struct argparse_ctx ctx = {
        .argc = argc,
        .argv = argv,
        .args = NULL,
        .end = false,
        .index = 1,
        .error = 0,
        .cmd = NULL,
        .argp = argp,
    };

    struct argparse_val *val;

    argp->prgname = argv[0];

    ctx.args = argparse_parse_tokenize(argp, argc, argv);
    // FIXME free args
    // FIXME quick fix
    ctx.argc = ctx.args->size;
    ctx.argv = (const char **)ctx.args->array;

    printf("[~] cmdline : ");
    for (int i = 0; i < ctx.args->size; ++i)
        printf("%s ", ctx.args->array[i]);
    printf("\n");

    argparse_val_vect_t positional;
    vector_create(positional, argparse_val_free);
    val = argparse_val(ARRAY, positional);
    hashtbl_set(ctx.argp->data, "args", val);

    while (ctx.index < ctx.argc && !ctx.end) {
        if (ctx.argv[ctx.index][0] == '-') {
            int res;
            if (ctx.argv[ctx.index][1] == '-')
                res = argparse_parse_long(&ctx);
                // if (!ctx.argv[ctx.index][2] && ctx.mood.endopt)
            else
                res = argparse_parse_short(&ctx);

            if (!res) {
                printf("[-] option: %s not found in %s\n", ctx.argv[ctx.index],
                       (ctx.cmd) ? ctx.cmd->name : "program");
                ctx.error = 1;
                break;
            } else if (res < 0) {
                printf("[-] option: %s error during process\n", ctx.argv[ctx.index]);
                ctx.error = 1;
                break;
            } else {
                ctx.index += res;
            }
        } else if (ctx.argp->commands && !ctx.cmd) {
            ctx.cmd = argparse_command_find(ctx.argp, ctx.argv[ctx.index]);
            if (!ctx.cmd) {
                printf("[-] command: %s not found\n", ctx.argv[ctx.index]);
                ctx.error = 1;
                break;
            } else {
                printf("[+] command: %s\n", ctx.cmd->name);
                val = argparse_val(STRING, ctx.cmd->name);
                hashtbl_set(ctx.argp->data, "command", val);
                ctx.index++;
            }
        } else {
            printf("[?] positional: %s\n", ctx.argv[ctx.index]);
            val = argparse_val(STRING, ctx.argv[ctx.index]);
            vector_add(positional, val);
            ctx.index++;
        }
    }

    if (ctx.error) {
        printf("[!] An error occured during the argument parsing\n"
               "[!] Please run %s --help to see the full features\n", argp->prgname);
        return -1;
    }

    if (argp->mood.version && argparse_data_exists(argp, "version")) {
        argp->mood.version(argp);
        return 1;
    }

    if (argp->mood.default_help && argparse_data_exists(argp, "help")) {
        if (ctx.cmd) {
            argparse_help(argp, ctx.cmd);
        } else {
            const char *res = NULL;
            res = argparse_data_array_at(argp, "help", 0);
            if (res) {
                struct argparse_cmd *cmd = argparse_command_find(argp, res);
                if (!cmd)
                    printf("[!] Unknown command %s\n", res);
                argparse_help(argp, cmd);
            } else {
                argparse_help(argp, NULL);
            }
        }
        return 1;
    }

    return 0;
}

// maybe add a printing context
void argparse_help_option(struct argparse_opt *opt, int deep)
{
    for (int i = 0; i < deep; ++i)
        putchar(' ');

    if (opt->shopt)
        printf("-%c, ", opt->shopt);
    else
        printf("    ");

    if (opt->longopt)
        printf("--%s", opt->longopt);

    printf("   %s\n", opt->help);
}

void argparse_help_command(struct argparse_cmd *cmd, int deep)
{
    int max_cmd_size = 10;
    printf("   %-*s %s\n", max_cmd_size, cmd->name, cmd->help);
    if (cmd->options) {
        struct argparse_opt **it_opt;
        vector_foreach(cmd->options, it_opt) {
            argparse_help_option(*it_opt, deep + 3);
        }
    }
}

void argparse_help(struct argparse* argp, struct argparse_cmd *cmd)
{
    if (!argp)
        return;

    if (argp->commands) {
        if (cmd) {
            printf("usage: %s %s [OPTIONS]\n", argp->prgname, cmd->name);
            printf("\nOptions:\n\n");
            argparse_help_command(cmd, 3);
            // FIXME: add help definition
            printf("\n");
            return;
        }
        printf("usage: %s <command> [OPTIONS]\n", argp->prgname);
        printf("\nCommands:\n\n");
        struct argparse_cmd **it_cmd;
        vector_foreach(argp->commands, it_cmd) {
            argparse_help_command(*it_cmd, 3);
        }
    } else {
        printf("usage: %s [OPTIONS]\n", argp->prgname);
    }

    if (argp->options) {
        printf("\nOptions:\n\n");
        struct argparse_opt **it_opt;
        vector_foreach(argp->options, it_opt)
            argparse_help_option(*it_opt, 3);
    }

    printf("\n");
}

bool argparse_data_exists(struct argparse *argp, const char *key)
{
    bool res;
    hashtbl_is_key(argp->data, key, res);
    return res;
}

const char *argparse_data_string(struct argparse *argp, const char *key)
{
    struct argparse_val *val = NULL;
    hashtbl_get(argp->data, key, val);
    if (!val)
        return NULL;
    return (const char *)val->data;
}

const char *argparse_data_array_at(struct argparse *argp, const char *key, size_t idx)
{
    struct argparse_val *val = NULL;
    hashtbl_get(argp->data, key, val);
    if (!val)
        return NULL;
    if (val->type != ARGPARSE_TYPE_ARRAY)
        return NULL;
    argparse_val_vect_t vect = val->data;
    if (idx >= vect->size)
        return NULL;
    val = vect->array[idx];

    // FIXME: val = NULL

    return (const char *)val->data;
}
