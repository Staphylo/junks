#ifndef VECTOR_H
# define VECTOR_H

# include <sys/types.h>

# define VECTOR_DECLARE(type, name)                                            \
    struct name##_s {                                                  \
        size_t size;                                                   \
        size_t maxSize;                                                \
        void   (*free)(type);                                          \
        type   *array;                                                 \
    };                                                                 \
    typedef struct name##_s *name##_t;

# define vector_create(vect, f)                                        \
    do {                                                               \
        vect = malloc(sizeof(*vect));                                  \
        vect->size = 0;                                                \
        vect->maxSize = 8;                                             \
        vect->free = f;                                                \
        vect->array = malloc(sizeof(*vect->array) * 8);                \
    }while(0)

# define vector_extend(vect)                                           \
    do {                                                               \
        vect->maxSize *= 2;                                            \
        vect->array = realloc(vect->array, sizeof(*vect->array) *      \
            vect->maxSize);                                            \
    }while(0)

# define vector_reduce(vect)                                           \
    do {                                                               \
        vect->maxSize /= 2;                                            \
        vect->array = realloc(vect->array, sizeof(*vect->array) *      \
            vect->maxSize);                                            \
    }while(0)

# define vector_add(vect, value)                                       \
    do {                                                               \
        if(vect->size == vect->maxSize)                                \
            vector_extend(vect);                                       \
        vect->array[vect->size++] = value;                             \
    }while(0)

# define vector_del(vect, n)                                           \
    do {                                                               \
        vect->array[n] = vect->array[vect->size-- - 1];                \
        if(vect->size < vect->maxSize / 2)                             \
            vector_reduce(vect);                                       \
    }while(0)

# define vector_del_order(vect, n)                                     \
    do {                                                               \
        size_t i;                                                      \
        for(i = n; i < vect->size - 1; ++i)                            \
            vect->array[i] = vect->array[i + 1];                       \
        --vect->size;                                                  \
    }while(0)

# define vector_free(vect)                                             \
    do {                                                               \
        size_t i;                                                      \
        if(vect->free)                                                 \
            for(i = 0; i < vect->size; ++i)                            \
                vect->free(vect->array[i]);                            \
        free(vect->array);                                             \
        free(vect);                                                    \
    }while(0)

# define vector_foreach_inc(vect, it)                                  \
    for(it = 0; it < vect->size; ++it)

# define vector_foreach(vect, it)                                      \
    for(it = vect->array; it != vect->array + vect->size; ++it)

#endif
