#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


#include "argparse.h"
#include "vector.h"

void version(struct argparse *argp)
{
    printf(
        "\n"
        "     Name : argparse test suite\n"
        "   Author : Samuel Angebault \"Staphylo\"\n"
        "  Version : 0.4 alpha\n"
        " Built on : " __DATE__ " @ " __TIME__ "\n"
        "\n");
};

int example(int argc, const char **argv)
{
    struct argparse_opt *opt;
    struct argparse_cmd *cmd;
    struct argparse *argp;

    argp = argparse(.version=version);

    opt = argparse_option(argp,
                          .shopt = 'h',
                          .longopt = "help",
                          .help = "Display this help message",
                          .params = 1,
                          .required = 0);

    opt = argparse_option(argp,
                          .shopt = 'v',
                          .longopt = "version",
                          .help = "Display the version of this software");

    cmd = argparse_command(argp,
                           .name = "discover",
                           .help = "Discover a device on the network");

    cmd = argparse_command(argp,
                           .name = "pair",
                           .help = "Pair to a device");

    opt = argparse_option(cmd,
                          .shopt = 'i',
                          .longopt = "id",
                          .help = "<id> Id of the device to pair to",
                          .params = 1,
                          .required = 1);

    cmd = argparse_command(argp,
                           .name = "unpair",
                           .help = "Unpair from a device");

    opt = argparse_option(cmd,
                          .longopt = "id",
                          .params = 1,
                          .required = 1);

    (void)opt;

    if (argparse_parse(argp, argc, argv) < 0) {
        goto error;
    }

    argparse_data_dump(argp);

    /*
    if (argparse_data_exists(argp, "version")) {
        goto end;
    }

    if (argparse_data_exists(argp, "help")) {
        // argparse_array_foreach
        // macro : argparse_data_get()
        // function : argparse_data_(int|char|string)
        //argparse_data_array(argp, "help");

        argparse_help(argp);
        goto end;
    }
    */

end:
    argparse_free(argp);
    return 0;

error:
    argparse_free(argp);
    return 1;
}

VECTOR_DECLARE(const char *, string_vect)

int test_parser(struct argparse *argp, ...)
{
    string_vect_t args;
    va_list ap;
    const char *param;

    vector_create(args, NULL);

    va_start(ap, argp);

    while ((param = va_arg(ap, const char *))) {
        vector_add(args, param);
    }

    va_end(ap);

    argparse_parse(argp, args->size, (const char**)args->array);

    vector_free(args);
}

#define test_parser(Argp, ...) test_parser(Argp, __VA_ARGS__, NULL)

int test_suite(void)
{
    struct argparse *argp;

    argp = argparse(/* behaviour */);
    test_parser(argp, "shopt", "-lat");

    argp = argparse(/* behaviour */);
    test_parser(argp, "shopt", "-ffile", "-ooutput", "-i" );
}

int main(int argc, const char **argv) {
    if (argc > 1)
        return example(argc, argv);
    return test_suite();
}

