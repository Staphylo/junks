#ifndef HASHTBL_H
# define HASHTBL_H

# include "list.h"

# define HASHTBL_DECLARE(KeyType, Type, Name)                          \
    struct Name##_item_s {                                             \
        KeyType key;                                                   \
        Type value;                                                    \
    };                                                                 \
    typedef struct Name##_item_s *Name##_item_t;                       \
    LIST_DECLARE(struct Name##_item_s, Name##_items);                  \
    struct Name##_s {                                                  \
        Name##_items_t *bucket;                                        \
        size_t size;                                                   \
        int(*hash)(KeyType);                                           \
        int(*cmp)(KeyType, KeyType);                                   \
        Name##_item_t it;                                              \
    };                                                                 \
    typedef struct Name##_s *Name##_t;

# define hashtbl_init(Table, Size, Hash, Cmp, Free)                    \
    do {                                                               \
        size_t i;                                                      \
        Table = malloc(sizeof(*Table));                                \
        Table->hash = Hash;                                            \
        Table->cmp = Cmp;                                              \
        Table->size = Size;                                            \
        Table->bucket = malloc(sizeof(*Table->bucket) * Size);         \
        for(i = 0; i < Size; ++i)                                      \
            list_init(Table->bucket[i], Free);                         \
    }while(0)

# define hashtbl_set(table, k, v)                                      \
    do {                                                               \
        int index = table->hash(k) % table->size;                      \
        table->it = malloc(sizeof(*table->it));                        \
        table->it->key = k;                                            \
        table->it->value = v;                                          \
        list_insert_head(table->bucket[index], *table->it);            \
        free(table->it);                                               \
    }while(0)

# define hashtbl_get(table, k, ret)                                    \
    do {                                                               \
        int index = table->hash(k) % table->size;                      \
        list_foreach(table->bucket[index]) {                           \
            if(!table->cmp(table->bucket[index]->it->data.key, k)) {   \
                ret = table->bucket[index]->it->data.value;            \
                break;                                                 \
            }                                                          \
        }                                                              \
    }while(0)

# define hashtbl_is_key(table, k, res)                                 \
    do {                                                               \
        int index = table->hash(k) % table->size;                      \
        res = false;                                                   \
        list_foreach(table->bucket[index]) {                           \
            if(!table->cmp(table->bucket[index]->it->data.key, k)) {   \
                res = true;                                            \
                break;                                                 \
            }                                                          \
        }                                                              \
    }while(0)

# define hashtbl_del(table, k)                                         \
    do {                                                               \
        int index = table->hash(k) % table->size;                      \
        list_foreach(table->bucket[index]) {                           \
            if(!table->cmp(table->bucket[index]->it->data.key, k)) {   \
                list_remove(table->bucket[index],                      \
                        table->bucket[index]->it);                     \
                break;                                                 \
            }                                                          \
        }                                                              \
    }while(0)

# define hashtbl_free(table)                                           \
    do {                                                               \
        size_t i;                                                      \
        for(i = 0; i < table->size; ++i)                               \
            list_free(table->bucket[i]);                               \
        free(table->bucket);                                           \
        free(table);                                                   \
    }while(0)

# define hashtbl_foreach(table)                                        \
    for(size_t i = 0; i < table->size; ++i)                            \
        list_foreach(table->bucket[i])                                 

# define hashtbl_debug(table)                                          \
    do {                                                               \
        size_t i;                                                      \
        for(i = 0; i < table->size; ++i) {                             \
            fprintf(stderr, "\n%ld : ", i);                            \
            list_foreach(table->bucket[i])                             \
                fprintf(stderr, "%s :  -> ",                           \
                        table->bucket[i]->it->data.key);               \
        }                                                              \
        fprintf(stderr, "\n");                                         \
    }while(0)

#endif
