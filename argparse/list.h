#ifndef LIST_H
# define LIST_H

# define LIST_DECLARE(type, name)                                      \
    typedef struct node_##name##_s *node_##name##_t;                   \
    struct node_##name##_s {                                           \
        type data;                                                     \
        node_##name##_t next;                                          \
        node_##name##_t prev;                                          \
    };                                                                 \
    struct name##_s {                                                  \
        unsigned size;                                                 \
        node_##name##_t head;                                          \
        node_##name##_t tail;                                          \
        node_##name##_t it;                                            \
        void(*free)(type);                                             \
    };                                                                 \
    typedef struct name##_s *name##_t;

# define list_init(list, Free)                                         \
    do {                                                               \
        list = calloc(1, sizeof(*list));                               \
        list->free = Free;                                             \
    }while(0)                                                          \

# define list_free(list)                                               \
    do{                                                                \
        while(list->head) {                                            \
            list->tail = list->head;                                   \
            list->head = list->head->next;                             \
            if(list->free)                                             \
                list->free(list->tail->data);                          \
            free(list->tail);                                          \
        }                                                              \
        free(list);                                                    \
    }while(0)

# define list_insert_after(list, val, node)                            \
    do {                                                               \
        if(node) {                                                     \
            void *nextNode = node->next;                               \
            node->next = malloc(sizeof(*node));                        \
            node->next->data = val;                                    \
            node->next->next = nextNode;                               \
            node->next->prev = node;                                   \
            if(node->next->next)                                       \
                node->next->next->prev = node->next;                   \
        }                                                              \
        else {                                                         \
            list->head = calloc(1, sizeof(*node));                     \
            list->head->data = val;                                    \
        }                                                              \
        if(node == list->tail)                                         \
            list->tail = node ? node->next : list->head;               \
        list->size++;                                                  \
    }while(0)

# define list_insert_before(list, val, node)                           \
    do {                                                               \
        if(node) {                                                     \
            void *prevNode = node->prev;                               \
            node->prev = malloc(sizeof(*node));                        \
            node->prev->data = val;                                    \
            node->prev->prev = prevNode;                               \
            node->prev->next = node;                                   \
            if(node->prev->prev)                                       \
                node->prev->prev->next = node->prev;                   \
        }                                                              \
        else {                                                         \
            list->tail = calloc(1, sizeof(*node));                     \
            list->tail->data = val;                                    \
        }                                                              \
        if(node == list->head)                                         \
            list->head = node ? node->prev : list->tail;               \
        list->size++;                                                  \
    }while(0)

# define list_insert_tail(list, val)                                   \
    do {                                                               \
        list_insert_after(list, val, list->tail);                      \
        if(!list->head)                                                \
            list->head = list->tail;                                   \
    }while(0)

# define list_insert_head(list, val)                                   \
    do {                                                               \
        list_insert_before(list, val, list->head);                     \
        if(!list->tail)                                                \
            list->tail = list->head;                                   \
    }while(0)

# define list_at(list, n, elt)                                         \
    do {                                                               \
        int i;                                                         \
        elt = list->head;                                              \
        for(i = 0; i < n && elt; ++i)                                  \
            elt = elt->next;                                           \
    }while(0)

# define list_foreach(list)                                            \
    for(list->it = list->head; list->it; list->it = list->it->next)

# define list_insert_at(list, val, n)                                  \
    do {                                                               \
        if(n < list->size) {                                           \
            list_at(list, n, list->it);                                \
            list_insert_before(list, val, list->it);                   \
        }                                                              \
        else                                                           \
            list_insert_tail(list, val);                               \
    }while(0)

# define list_remove(list, node)                                       \
    do {                                                               \
        if(node->next)                                                 \
            node->next->prev = node->prev;                             \
        if(node->prev)                                                 \
            node->prev->next = node->next;                             \
        if(node == list->head)                                         \
            list->head = node->next;                                   \
        if(node == list->tail)                                         \
            list->tail = node->prev;                                   \
        if(list->free)                                                 \
            list->free(node->data);                                    \
        free(node);                                                    \
        list->size--;                                                  \
    }while(0)

# define list_remove_at(list, n)                                       \
    do {                                                               \
        if(n < list->size) {                                           \
            list_at(list, n, list->it);                                \
            list_remove(list, list->it);                               \
        }                                                              \
    }while(0)

# define list_remove_head(list)                                        \
    do {                                                               \
        list->it = list->head;                                         \
        list_remove(list, list->it);                                   \
    }while(0)

#endif
