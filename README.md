Junks
=====

This repo contains unfinished work and tests.

debug
-----

debug tools for a program:

 - tweakable variables
 - show unfreed blocks


fcount
------

Fancy count
a kind of find | wc combo


maze
----

a simple maze project with generation and solving via Dijkstra


mygetopt
--------

a getopt that has more possibilities than the standard one

coroutine
---------

coroutine library in C
