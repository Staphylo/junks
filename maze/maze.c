#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>

/* Some typedefs */
typedef struct cell_s* cell_t;
typedef struct cell_list_s* cell_list_t;
typedef struct maze_s* maze_t;
typedef unsigned char byte;

/* cell data */
struct cell_s {
    cell_list_t neighbors;
    cell_list_t linked;
    u_int8_t visited;
    u_int8_t solution;
};

/* Cell list */
struct cell_list_s {
    cell_t cell;
    cell_list_t next;
};

// Maze informations
struct maze_s {
    cell_t **grid;
    int width;
    int height;
};

static void cell_list_add(cell_list_t *list, cell_t item)
{
    cell_list_t prec, tmp;

    tmp = *list;
    prec = tmp;

    while(tmp)
    {
        prec = tmp;
        tmp = tmp->next;
    }

    tmp = malloc(sizeof(cell_list_t));
    tmp->cell = item;
    tmp->next = NULL;

    if(!*list)
        *list = tmp;
    else
        prec->next = tmp;
}

static int cell_list_find(cell_list_t list, cell_t cell)
{
    while(list)
    {
        if(list->cell == cell)
            return 0;
        list = list->next;
    }

    return -1;
}

static void cell_list_swap(cell_list_t list, int first, int second)
{
    cell_list_t f = NULL;
    cell_list_t s = NULL;
    cell_t tmp = NULL;
    int i = 0;

    while(list)
    {
        if(i == first)
            f = list;
        if(i == second)
            s = list;
        list = list->next;
        i++;
    }

    if(f && s && f != s)
    {
        tmp = s->cell;
        s->cell = f->cell;
        f->cell = tmp;
    }
}

static int cell_list_len(cell_list_t list)
{
    int i = 0;
    while(list)
    {
        i++;
        list = list->next;
    }

    return i;
}

/* generate a random int in the range given */
static int my_rand(int min, int max)
{
    return rand() % (max-min) + min;
}

/* randomize places in the array of neighbors */
static void randomize_neighbors(cell_list_t nei)
{
    int i, len;
    len = cell_list_len(nei);
    for(i = 0; i < len; i++)
        cell_list_swap(nei, i, my_rand(0, len));
}

/* initialize a maze structure */
maze_t init_maze(int width, int height)
{
    maze_t maze;
    int i,j;

    maze = malloc(sizeof(maze_t));
    maze->width = width;
    maze->height = height;
    maze->grid = malloc(width * sizeof(cell_t));

    for(i = 0; i < width; i++)
    {
        maze->grid[i] = malloc(height * sizeof(cell_t));
        for(j = 0; j < height; j++)
        {
            maze->grid[i][j] = malloc(sizeof(cell_t));
            maze->grid[i][j]->visited = 0;
            maze->grid[i][j]->solution = 0;
            maze->grid[i][j]->neighbors = NULL;
            maze->grid[i][j]->linked = NULL;
        }
    }

    return maze;
}

/* free properly the maze structure and its data */
static void free_maze(maze_t maze)
{
    int i;
    if(maze)
    {
        if(maze->grid)
        {
            for(i = 0; i < maze->width; i++)
                free(maze->grid[i]);
            free(maze->grid);
        }
        free(maze);
    }
}

/* recursively generate a path */
static void generate(cell_t cell)
{
    cell_list_t tmp;
    cell->visited = 1;
    randomize_neighbors(cell->neighbors);
    tmp = cell->neighbors;
    while(tmp)
    {
        if(!tmp->cell->visited)
        {
            cell_list_add(&cell->linked, tmp->cell);
            cell_list_add(&tmp->cell->linked, cell);
            generate(tmp->cell);
        }
        tmp = tmp->next;
    }
}

/* hat function to generate a maze */
static void generate_maze(maze_t maze)
{
    cell_t tmp = NULL;
    for(int i = 0; i < maze->width; i++)
    {
        for(int j = 0; j < maze->height; j++)
        {
            tmp = maze->grid[i][j];

            if(i > 0)
                cell_list_add(&tmp->neighbors, maze->grid[i-1][j]);
            if(i < maze->width-1)
                cell_list_add(&tmp->neighbors, maze->grid[i+1][j]);
            if(j > 0)
                cell_list_add(&tmp->neighbors, maze->grid[i][j-1]);
            if(j < maze->height-1)
                cell_list_add(&tmp->neighbors, maze->grid[i][j+1]);
        }
    }

    generate(maze->grid[my_rand(0, maze->width)][my_rand(0, maze->height)]);
}

/* printing the maze */
static void print_maze(maze_t maze)
{
    int i, j;
    printf(".");
    for(i = 0; i < maze->width; i++)
        printf("_.");
    printf("\n");
    for(j = 0; j < maze->height; j++)
    {
        printf("|");
        for(i = 0; i < maze->width; i++)
        {
            if(maze->grid[i][j]->solution)
                printf("\033[41m");

            if(j == maze->height - 1 ||
                    cell_list_find(maze->grid[i][j]->linked,
                        maze->grid[i][j+1]) == -1)
                printf("_");
            else
                printf(" ");

            if(i == maze->width - 1||
                    cell_list_find(maze->grid[i][j]->linked,
                        maze->grid[i+1][j]) == -1)
                printf("|");
            else
                printf(".");

            if(maze->grid[i][j]->solution)
                printf("\033[00m");

        }
        printf("\n");
    }
}


enum place
{
    wall  = 0x00,
    empty = 0x01,
    path  = 0x02
};

/* lol? */
static void print_maze_in_an_other_way(maze_t maze)
{
    byte **array;
    array = calloc(maze->width*2 + 1, sizeof(byte *));

    for(int i = 0; i < maze->width*2+1; i++)
        array[i] = calloc(maze->height*3+1, sizeof(byte));

    for(int i = 1, x = 0; i < maze->width*2; i+=2, x++)
    {
        for(int j = 1, y = 0; j < maze->height*3; j+=3, y++)
        {
            if(maze->grid[x][y]->solution)
                array[i][j] = path;
            else
                array[i][j] = empty;

            if(y < maze->height - 1 &&
                    cell_list_find(maze->grid[x][y]->linked,
                        maze->grid[x][y+1]) != -1)
            {
                if (maze->grid[x][y+1]->solution && maze->grid[x][y]->solution)
                {
                    array[i][j+1] = path;
                    array[i][j+2] = path;
                }
                else
                {
                    array[i][j+1] = empty;
                    array[i][j+2] = empty;
                }
            }
            if(x < maze->width - 1 &&
                    cell_list_find(maze->grid[x][y]->linked,
                        maze->grid[x+1][y]) != -1)
            {
                if (maze->grid[x+1][y]->solution && maze->grid[x][y]->solution)
                    array[i+1][j] = path;
                else
                    array[i+1][j] = empty;
            }
        }
    }

    for(int i = 0; i < maze->width*2 + 1; i++)
    {
        for(int j = 0; j < maze->height*3 + 1; j++)
        {
            switch(array[i][j])
            {
                case empty:
                    putchar(' ');
                    break;
                case path:
                    printf("\033[41m \033[00m");
                    break;
                case wall:
                    printf("\033[47m \033[00m");
                    break;
            }
        }
        putchar('\n');
        free(array[i]);
    }
    free(array);
}

/* function reseting information on each case */
static void unset_flags(maze_t maze)
{
    int i, j;
    for(i = 0; i < maze->width; i++)
    {
        for(j = 0; j < maze->height; j++)
        {
            maze->grid[i][j]->visited = 0;
            maze->grid[i][j]->solution = 0;
        }
    }
}

/* recursive function solving a maze */
static int solve(cell_t cur, cell_t end)
{
    cell_list_t lnk;
    if(end == cur)
    {
        end->solution = 1;
        end->visited = 1;
        return 1;
    }

    lnk = cur->linked;
    while(lnk)
    {
        cur->visited = 1;
        if(!lnk->cell->visited && solve(lnk->cell, end))
        {
            cur->solution = 1;
            return 1;
        }
        lnk = lnk->next;
    }

    return 0;
}

/* hat function for solving a maze */
static int solve_maze(maze_t maze)
{
    unset_flags(maze);
    solve(maze->grid[0][0], maze->grid[maze->width-1][maze->height-1]);
    return 0;
}

/* generating solving and printing atm */
static void main_menu(int width, int height)
{
    maze_t maze = init_maze(width, height);
    generate_maze(maze);
    //print_maze(maze);
    solve_maze(maze);
    print_maze(maze);
    print_maze_in_an_other_way(maze);
    free_maze(maze);
}

/* program entry point */
int main(int argc, char **argv)
{
    if(argc < 3 )
    {
        printf("%s <width> <height>\n", argv[0]);
        return EXIT_SUCCESS;
    }

    srand(time(NULL));
    main_menu(atoi(argv[1]), atoi(argv[2]));

    return EXIT_SUCCESS;
}
