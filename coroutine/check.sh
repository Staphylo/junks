#!/bin/sh

function log {
    color=""
    reset="\e[0m"
    case $1 in
        @) color="\e[1;34m" ;;
        -) color="\e[1;31m" ;;
        +) color="\e[1;32m" ;;
    esac
    if [ -z "$color" ]; then
        echo $@
    else
        local sym=$1
        shift
        echo -e "$color[$sym]$reset $@"
    fi
}


timeout=5
bin=$1
shift

if [ -z "$bin" ]; then
    echo "usage: $0 <bin> [args...]"
    exit 1
fi

if [ ! -f "$bin.expect" ]; then
    log - "Expecting reference file $bin.expect"
    exit 1
fi

log @ "testing $bin...";
outfile=`mktemp`;

timeout $timeout $bin 2> /dev/null > $outfile;
res=$?
if [ $res -eq 124 ]; then
    log - "TIMEOUT"
elif [ $res -gt 128 ]; then
    log - "FAIL signal $(($res - 128)) received"
elif [ $res -ne 0 ]; then
    log - "FAIL non zero return value $res"
elif diff $outfile $bin.expect; then
    log + "PASS"
else
    log - "FAIL"
fi

rm -f $outfile
