#include <stdio.h>
#include <coroutine/coroutine.h>

int main()
{
    coroutine_init();
    coroutine((int i) {
        printf("having %d\n", i );
    }, 42);
    coroutine_run();
    return 0;
}
