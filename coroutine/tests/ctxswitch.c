#include <stdio.h>
#include <coroutine/coroutine.h>

int main()
{
    coroutine_init();
    coroutine((unsigned count) {
        while (count--) {
            coroutine_yield();
        }
    }, 1000000);
    coroutine_run();
    return 0;
}
