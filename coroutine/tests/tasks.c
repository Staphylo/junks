#define _GNU_SOURCE
#include <coroutine/coroutine.h>
#include <coroutine/task.h>

#include <unistd.h>

struct test_task_data {
    int value;
};

void *test_task(void *ptr) {
    struct test_task_data *data = ptr;
    sleep(data->value);
    return (void*)(uintptr_t)data->value;
}

int main()
{
    coroutine_init();
    coroutine_task_init(4); // XXX or flags: coroutine_init(COROUTINE_INIT_ALL)

    int task_count = 4;
    for (int i = 1; i <= task_count; ++i) {
        coroutine((int num) {
            struct test_task_data data = {
                .value = num
            };
            //void *res = coroutine_task((int value) {
            //}, );
            void *res = coroutine_task_run(test_task, &data);
            printf("thread waited for %" PRIuPTR "s\n", (uintptr_t)res);
        }, i);
    }

    // FIXME add a timer here to check that the thread yield correctly
    coroutine_run();
    return 0;
}
