#include <stdio.h>
#include <coroutine/coroutine.h>

int main()
{
    coroutine_init();

    coroutine(() {
        puts("[1] this");
        coroutine_yield();
        puts("[1] a");
        coroutine_yield();
        puts("[1] for");
        coroutine_yield();
        puts("[1] scheduler");
    });

    coroutine(() {
        puts("[2] is");
        coroutine_yield();
        puts("[2] test");
        coroutine_yield();
        puts("[2] the");
    });

    coroutine_run();
    return 0;
}
