#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <coroutine/coroutine.h>

struct test {
    int counter100;
    int counter500;
};

int main()
{
    struct test test = {
        0,
        0
    };

    coroutine_init();

    coroutine((volatile struct test *test) {
        while (test->counter100 < 10) {
            puts("increasing 100");
            test->counter100++;
            coroutine_wait_ms( 100 );
        }
    }, &test);

    coroutine((volatile struct test *test) {
        while (test->counter500 < 3) {
            puts("increasing 500");
            test->counter500++;
            coroutine_wait_ms( 500 );
        }
    }, &test);

    /*
    coroutine_every(500, (volatile struct test *test) {
        test->counter500++;
    }, &test);
    */

    coroutine((volatile struct test *test) {
        int count = 0;
        while (count < 3) {
            printf("[%d] c1: %d, c2: %d\n", count,
                   test->counter100, test->counter500);
            count++;
            coroutine_wait_ms( 500 );
        }
    }, &test);

    coroutine_run();

    return 0;
}
