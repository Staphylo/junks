#include <stdio.h>
#include <coroutine/coroutine.h>

int main()
{
    // XXX: avoid init by using __attribute__((constructor))
    coroutine_init();
    coroutine((int a) {
        printf("%d\n", a);
    }, 1);
    coroutine((int a, int b) {
        printf("%d %d\n", a, b);
    }, 1, 2);
    coroutine((int a, int b, int c) {
        printf("%d %d %d\n", a, b, c);
    }, 1, 2, 3);
    coroutine((int a, int b, int c, int d) {
        printf("%d %d %d %d\n", a, b, c , d);
    }, 1, 2, 3, 4);
    coroutine((int a, int b, int c, int d, int e) {
        printf("%d %d %d %d %d\n", a, b, c, d, e);
    }, 1, 2, 3, 4, 5);
    coroutine((int a, int b, int c, int d, int e, int f) {
        printf("%d %d %d %d %d %d\n", a, b, c, d, e, f);
    }, 1, 2, 3, 4, 5, 6);
    coroutine((int a, int b, int c, int d, int e, int f, int g) {
        printf("%d %d %d %d %d %d %d\n", a, b, c, d, e, f, g);
    }, 1, 2, 3, 4, 5, 6, 7);
    coroutine_run();
    return 0;
}
