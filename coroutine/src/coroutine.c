#define _GNU_SOURCE
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/epoll.h>

#include <coroutine/log.h>
#include <coroutine/context.h>
#include <coroutine/coroutine.h>

// See later for tls
// runtime behaviour : bool tls = true / false return the tls or global one
// or compile time : #ifdef USE_TLS on coroutine_instance
struct coroutine_manager coroutine_manager;

bool coroutine_init()
{
    struct coroutine_manager *cm = coroutine_instance();
    memset(cm, 0, sizeof (*cm));
    return coroutine_threads_init(&cm->threads);
}

// 
// saving current stack and current registers
// executing thread_1 with new stack and new registers
// yield implying saving thread_1 registers and changing stack and register
// with scheduler ones
// then find a new process to exec
// etc...
//

const char *coroutine_return_strs[] = {
    [COROUTINE_RETURN_OK]       = "OK",
    [COROUTINE_RETURN_EXIT]     = "EXIT",   // remove coroutine
    [COROUTINE_RETURN_YIELD]    = "YIELD",  // schedule next entity
    [COROUTINE_RETURN_IO]       = "IO",     // epoll / asyncio
    [COROUTINE_RETURN_IO_READ]  = "IOREAD",
    [COROUTINE_RETURN_IO_WRITE] = "IOWRITE",
    [COROUTINE_RETURN_TIMER]    = "TIMER",  // clock_gettime / timerfd
    [COROUTINE_RETURN_TASK]     = "TASK",
};

static const char * coroutine_return_str(enum coroutine_return  code)
{
    assert( code < sizeof(coroutine_return_strs) / sizeof(const char *) );
    return coroutine_return_strs[code];
}

static int coroutine_context_switch(struct coroutine_ctx *from,
                                    const struct coroutine_ctx *to,
                                    int code)
{
    int res = coroutine_context_save(from);

    if (res == 0) {
        log(LOG_DEBUG, "ctx-switch: from %p =[%s]=> to %p", from,
                                            coroutine_return_str(code), to);
        coroutine_context_restore(to, code);
        // previous statement doesn't return
    }
    else
        log(LOG_DEBUG, "ctx-switch: restored with code %s",
                                            coroutine_return_str(res));

    return res;
}

#if 0
static void coroutine_alarm(int sig)
{
    // check current context
    // if current context is the scheduler then return
    // else context switch with the scheduler

    // XXX fixme hard
    struct coroutine_manager *cm = coroutine_instance();
    if (cm->current != &cm->scheduler) {
        coroutine_context_switch(&cm->current->ctx, &cm->scheduler.ctx,
                                 COROUTINE_RETURN_TIMER);
    }
}
#endif

static bool coroutine_io_update(struct coroutine_manager *cm, bool block)
{
    int nfds = 0;
    struct epoll_event events[COROUTINE_EPOLL_QUEUE];

    if (block) {
        // TODO create timerfd for timers
        // TODO create eventfd for events
    }

    do
    {
        nfds = epoll_wait(cm->io.epollfd, events, COROUTINE_EPOLL_QUEUE,
                          (block) ? -1 : 0);
        for (int i = 0; i < nfds; ++i) {
            struct epoll_event *ev = events + i;
            struct coroutine_thread *th = ev->data.ptr;
            // signalfd handling
            // ev->data.poll
            // ev->data.ptr;
            if (ev->events & EPOLLHUP)
            {
                // FIXME socket error
                log(LOG_NOTICE, "epoll_wait hangup fd %d", th->fd);
                close(th->fd);
            }
            else if (ev->events & EPOLLERR)
            {
                // FIXME socket error
                log(LOG_ERROR, "epoll_wait error on fd %d", th->fd);
                close(th->fd);
            }
            else if (ev->events & EPOLLOUT) {
                //
            }
            else if (ev->events & EPOLLIN) {
                th->flags &= ~(COROUTINE_FLAG_WAITING);
                cm->threads.active++;
            }
            else
                log(LOG_WARN, "epoll_wait unhandled with event %d", ev->events);
        }
    } while (nfds == COROUTINE_EPOLL_QUEUE);

    return true;
}

static bool coroutine_io_watch(struct coroutine_manager *cm,
                               struct coroutine_thread *th) // event?
{
    struct epoll_event ev;

    /*ev.events = EPOLLIN | EPOLLOUT | EPOLLET | EPOLLONESHOT;*/
    ev.events = EPOLLIN | EPOLLET;
    ev.data.ptr = th;

    if (epoll_ctl(cm->io.epollfd, EPOLL_CTL_ADD, th->fd, &ev)) {
        // FIXME avoid useless syscall
        if (errno == EEXIST)
            return true;
        log_errno("epoll_ctl");
        return false;
    }

    return true;
}

static bool coroutine_io_init(struct coroutine_manager *cm)
{
    cm->io.epollfd = epoll_create1(0);
    if (cm->io.epollfd == -1) {
        log_errno("epoll_create1");
        return false;
    }

    return true;
}

#if 0
static void coroutine_wait_for_event(struct coroutine_manager *cm) {
    struct timespec ts;
    //if ( coroutine_when(&ts) ) {
        // add timerfd
    //}
    // wait for io
}
#endif

static void coroutine_scheduler(struct coroutine_manager *cm)
{
    cm->is_running = true;

    // sigaction
    // setitimer
    // what to do for io

    cthread_s *old = cm->current;

    while (cm->is_running && cm->threads.count)
    {
        cthread_s *new;
        /*
        while (!(new = coroutine_threads_next(&cm->threads, old))) {
            coroutine_io_update(cm, cm->threads.active == 0);
            // coroutine_wait_for_event();
            if (old == new && !coroutine_thread_ready(new)) {
                usleep(10); // XXX fix this ugly thing
                continue;
            }
        }
        */

        while (true) {
            coroutine_threads_dump(&cm->threads);
            new = coroutine_threads_next(&cm->threads, old);
            if ( !new ) {
                log(LOG_DEBUG, "no more entity to schedule");
                return;
            }
            if ( new == old && !coroutine_thread_ready(new)) {
                // coroutine_wait_for_event();
                coroutine_io_update(cm, cm->threads.active == 0);
                /*usleep(10);*/
                /*usleep(10);*/
                continue;
            }
            break;
        }

        if ( new < old )
            coroutine_io_update(cm, cm->threads.active == 0);

        cm->current = new;

        struct coroutine_ctx *from = &cm->scheduler.ctx;
        struct coroutine_ctx *to = &new->ctx;

        old = new;

        size_t thid = new - cm->threads.threads;
        log(LOG_NOTICE, "scheduling Thread(%zu)", thid);
        int res = coroutine_context_switch(from, to, COROUTINE_RETURN_OK);

        // the thread has return with an exit status
        // FIXME: waiting flag is enough for a lot of the operations
        //        comparison on multiple flags should be avoided
        switch (res)
        {
            // FIXME: useless call to epoll when waiting for an already
            // existant socket
            // is send gonna block due to a lack of notification?
            case COROUTINE_RETURN_IO_READ:
            case COROUTINE_RETURN_IO_WRITE:
            case COROUTINE_RETURN_IO:
                log(LOG_NOTICE, "scheduling: IOThread(%zu)", thid);
                cm->threads.active--;
                // FIXME only if one shot or not previously added
                coroutine_io_watch(cm, new);
                break;
            case COROUTINE_RETURN_TIMER:
                log(LOG_WARN, "timer not handled");
                // FIXME if there are no more thread to schedule a timerfd
                // should be created on the earlier timer possible
                cm->threads.active--;
                break;
            case COROUTINE_RETURN_TASK:
                log(LOG_NOTICE, "waiting for task to complete");
                // FIXME if there are no more thread to schedule an eventfd
                // should be created to notify of a thread completion
                //cm->threads.active--;
                break;
            case COROUTINE_RETURN_EXIT:
                log(LOG_NOTICE, "scheduling ExitThread(%zu)", thid);
                coroutine_threads_remove(&cm->threads, new);
                break;
        }
    }
}

// should never been called but whatever
void coroutine_exit()
{
    struct coroutine_manager *cm = coroutine_instance();

    /*for (size_t i = 0; i < cm->count; ++i) {*/
    /*    munmap(cm->threads[i].stack, cm->threads[i].stack_size);*/
    /*}*/

    /*free(cm->threads);*/
    memset(cm, 0, sizeof (*cm));
}

void coroutine_run()
{
    struct coroutine_manager *cm = coroutine_instance();

    if (cm->threads.count == 0) {
        log(LOG_ERROR, "coroutine system must be fed with at least 1 thread");
        abort();
    }

    if (!coroutine_signal_init(&cm->signal)) {
        log(LOG_ERROR, "failed to init signal handling");
        abort();
    }

    if (!coroutine_io_init(cm)) {
        log(LOG_ERROR, "failed to init io handling");
        abort();
    }

    log(LOG_NOTICE, "coroutine: run");
    coroutine_scheduler(cm);
    log(LOG_NOTICE, "coroutine: nothing to run anymore");
    coroutine_exit();
}

void coroutine_yield_with(int status)
{
    struct coroutine_manager *cm = coroutine_instance();

    // will context switch with the scheduler
    struct coroutine_ctx *from = &cm->current->ctx;
    struct coroutine_ctx *to = &cm->scheduler.ctx;

    coroutine_context_switch(from, to, status);
}

void __noreturn coroutine_end(void)
{
    struct coroutine_manager *cm = coroutine_instance();

    log(LOG_DEBUG, "thread: end stub => %p", &cm->scheduler.ctx);

    // no returning call
    coroutine_context_restore(&cm->scheduler.ctx, COROUTINE_RETURN_EXIT);

    log(LOG_ERROR, "This code portion should have never been called");
    abort();
}


void coroutine_create(coroutine_f func, void *data)
{
    coroutine_vadd(func, 1, data);
    //th->data = data;
}

void coroutine_vadd(coroutine_f func, size_t args, ...)
{
    va_list ap;
    struct coroutine_manager *cm = coroutine_instance();

    // thread init
    struct coroutine_thread *th = coroutine_threads_new(&cm->threads);

    th->stack_size = COROUTINE_STACK_SIZE;
    th->stack = mmap(NULL, th->stack_size, PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    th->func = func;

    // exec context init
    struct coroutine_ctx *ctx = &th->ctx;
    // XXX avoid coroutine_context_init?
    coroutine_context_init(ctx, th->stack, th->stack_size, coroutine_end);

    va_start(ap, args);
    coroutine_context_make(ctx, func, args, &ap);
    va_end(ap);

    log(LOG_NOTICE, "creating Thread(%zu)", th - cm->threads.threads);
}

