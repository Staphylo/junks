#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>

#include <coroutine/log.h>
#include <coroutine/threads.h>

bool coroutine_threads_init(struct coroutine_threads *threads)
{
    memset(threads, 0, sizeof (*threads));
    threads->size = COROUTINE_THREAD_ALLOC_SIZE;
    threads->threads = calloc(COROUTINE_THREAD_ALLOC_SIZE,
                              sizeof (struct coroutine_thread));

    return threads->threads;
}

struct coroutine_thread *coroutine_threads_new(struct coroutine_threads *threads)
{
    struct coroutine_thread *new = NULL;

    if (threads->count == threads->size) {
        threads->size += COROUTINE_THREAD_ALLOC_SIZE;
        void *res = realloc(threads->threads, threads->size); // XXX error incomming
        if (!res) {
            threads->size -= COROUTINE_THREAD_ALLOC_SIZE;
            log_errno("Failed to realloc");
            return NULL;
        }
        threads->threads = res;
        threads->last = threads->count + 1;
        new = &threads->threads[threads->count];
    }
    else {
        size_t i = 0;
        for (; i < threads->size; ++i)
            if (!(threads->threads[i].flags & COROUTINE_FLAG_USED))
                break;

        if (i >= threads->last)
            threads->last = i + 1;

        new = &threads->threads[i];
    }

    threads->count++;
    threads->active++;
    new->fd = -1;
    new->flags = COROUTINE_FLAG_USED;

    return new;
}

void coroutine_threads_remove(struct coroutine_threads *threads, struct coroutine_thread *th)
{
    size_t id = th - threads->threads;
    if (id == threads->last - 1) {
        while (!(threads->threads[id].flags & COROUTINE_FLAG_USED))
            id--;
        threads->last = id + 1;
    }
    threads->count--; // change last if possible
    threads->active--;
    th->flags = COROUTINE_FLAG_EMPTY;
}

void coroutine_threads_dump(struct coroutine_threads *threads)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_COARSE, &ts);
    for (size_t i = 0; i < threads->last; ++i) {
        struct coroutine_thread *th = &threads->threads[i];
        if (!(th->flags & COROUTINE_FLAG_USED)) {
            log(LOG_DEBUG, "Thread %zu: unused", i);
            continue;
        }
        if (th->flags == COROUTINE_FLAG_USED) {
            log(LOG_DEBUG, "Thread %zu: running", i);
            continue;
        }

        if (th->flags & COROUTINE_FLAG_WAITING) {
            log(LOG_DEBUG, "Thread %zu: waiting", i);
        }
        if (th->flags & COROUTINE_FLAG_TIMER) {
            log(LOG_DEBUG, "Thread %zu: waiting for %ld secs, %ld ns", i,
                th->wait.tv_sec - ts.tv_sec,
                th->wait.tv_nsec - ts.tv_nsec);
        }
        if (th->flags & COROUTINE_FLAG_TASK) {
            log(LOG_DEBUG, "Thread %zu: waiting for task", i);
        }
    }
}

struct coroutine_thread *coroutine_threads_next(struct coroutine_threads *threads, struct coroutine_thread *current)
{
    if (!current)
        return &threads->threads[0];

    if (threads->count == 0)
        return NULL;

    size_t c = current - threads->threads;
    struct timespec ts = { 0, 0 };
    for (size_t i = 0; i < threads->last; ++i) {
        c = (c + 1) % threads->last;
        struct coroutine_thread *th = &threads->threads[c];

        if (!(th->flags & COROUTINE_FLAG_USED))
            continue;

        if (th->flags & COROUTINE_FLAG_WAITING ||
            th->flags & COROUTINE_FLAG_TASK) {
            // handled in the scheduler that calls io_wait
#if 0
            int res = aio_error(&th->aiocb);
            if (res == EINPROGRESS)
                continue;
            /*else if (res == 0) {*/
            else {
                th->flags &= ~(COROUTINE_FLAG_WAITING);
                break;
            //} else {
                // set return value to be -1
            }
#endif
        }
        else if (th->flags & COROUTINE_FLAG_TIMER) {
            if (ts.tv_sec == 0) {
                // XXX set compile time flag for the clock to choose
                // here unprecise clock
                if ( clock_gettime(CLOCK_MONOTONIC_COARSE, &ts) == -1 ) {
                    log(LOG_ERROR, "failed to get clock");
                    // FIXME: handle error case
                }
            }
            if (ts.tv_sec > th->wait.tv_sec ||
                (ts.tv_sec == th->wait.tv_sec && ts.tv_nsec > th->wait.tv_nsec)) {
                th->flags &= ~(COROUTINE_FLAG_TIMER);
                log(LOG_DEBUG, "timer: ~ %ldns",
                    (ts.tv_sec - th->wait.tv_sec) * 1000000000 +
                    (ts.tv_nsec - th->wait.tv_nsec));
                break;
            }
#if 0
            if (tv.tv_sec == 0)
                gettimeofday(&tv, NULL);
            if (tv.tv_sec > th->wait.tv_sec ||
                (tv.tv_sec == th->wait.tv_sec && tv.tv_usec > th->wait.tv_usec)) {
                th->flags &= ~(COROUTINE_FLAG_TIMER);
                log(LOG_DEBUG, "timer: ~ %ldus",
                    (tv.tv_sec - th->wait.tv_sec) * 1000000 + (tv.tv_usec - th->wait.tv_usec));
                break;
            }
            continue;
#endif
        }
        else {
            break;
        }
    }

    return &threads->threads[c];
}
