#define _GNU_SOURCE
#include <time.h>

#include <coroutine/log.h>
#include <coroutine/coroutine.h>

#include <coroutine/timer.h>

static const char * coroutine_time_str(uint64_t ns)
{
    static char buffer[10];
    const char *label;
    uint64_t value;

    if (ns >= 1000000000) {
        label = "s";
        value = ns / 1000000000;
    }
    else if (ns >= 1000000) {
        label = "ms";
        value = ns / 1000000;
    }
    else if (ns >= 1000) {
        label = "us";
        value = ns / 1000;
    }
    else {
        label = "ns";
        value = ns;
    }

    snprintf(buffer, sizeof(buffer), "%" PRIu64 "%s", value, label);
    return buffer;
}

void coroutine_wait_ns(uint64_t ns)
{
    // coroutine_timer_set(coroutine_current_thread(), us);
    struct coroutine_thread *th = coroutine_current_thread();

    clock_gettime(CLOCK_MONOTONIC_COARSE, &th->wait);

    size_t tmp = th->wait.tv_nsec + ns;
    th->wait.tv_sec += tmp / 1000000000;
    th->wait.tv_nsec = tmp % 1000000000;

    th->flags |= COROUTINE_FLAG_TIMER;

    log(LOG_DEBUG, "wait: for %s", coroutine_time_str(ns));
    coroutine_yield_with(COROUTINE_RETURN_TIMER);
}

/*static bool handle_timer( struct coroutine_thread *th ) {
    return true;
}*/

/*static __constructor void init() {
    coroutine_register(COROUTINE_RETURN_TIMER, "TIMER", handle_timer);
}*/

