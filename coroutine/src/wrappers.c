#define _GNU_SOURCE
#include <sys/socket.h>
#include <unistd.h>

#include <coroutine/coroutine.h>
#include <coroutine/wrappers.h>

#if 0
ssize_t coroutine_read(int fd, void *buf, size_t size)
{
    struct coroutine_thread *th = coroutine_current_thread();

    th->aiocb.aio_fildes = fd;
    th->aiocb.aio_buf = buf;
    th->aiocb.aio_nbytes = size;
    th->aiocb.aio_sigevent.sigev_notify = SIGEV_NONE;

    th->flags |= COROUTINE_FLAG_WAITING;

    int ret = aio_read(&th->aiocb);
    if (ret < 0)
        return ret;

    th->aiocb.aio_offset += size;

    coroutine_yield();

    return aio_return(&th->aiocb);
}

ssize_t coroutine_write(int fd, const void *buf, size_t size)
{
    struct coroutine_thread *th = coroutine_current_thread();

    th->aiocb.aio_fildes = fd;
    th->aiocb.aio_buf = (void *)buf;
    th->aiocb.aio_nbytes = size;
    th->aiocb.aio_sigevent.sigev_notify = SIGEV_NONE;

    th->flags |= COROUTINE_FLAG_WAITING;

    int ret = aio_write(&th->aiocb);
    if (ret < 0)
        return ret;

    th->aiocb.aio_offset += size;

    coroutine_yield();

    return aio_return(&th->aiocb);
}
#endif

void coroutine_wait_for_socket(int socket, int operation)
{
    struct coroutine_thread *th = coroutine_current_thread();
    th->flags |= COROUTINE_FLAG_WAITING;
    th->fd = socket;
    coroutine_yield_with(operation);
}

ssize_t coroutine_read(int fd, void *buf, size_t size)
{
    struct coroutine_thread *th = coroutine_current_thread();
    th->flags |= COROUTINE_FLAG_WAITING;
    // TODO: asyncio instead
    coroutine_yield();
    return read(fd, buf, size);
}

ssize_t coroutine_write(int fd, const void *buf, size_t size)
{
    struct coroutine_thread *th = coroutine_current_thread();
    th->flags |= COROUTINE_FLAG_WAITING;
    // TODO: asyncio instead
    coroutine_yield();
    /*coroutine_yield(COROUTINE_RETURN_IO);*/
    return write(fd, buf, size);
}

ssize_t coroutine_recv(int socket, void *buffer, size_t length)
{
    coroutine_wait_for_socket(socket, COROUTINE_RETURN_IO_READ);
    // FIXME: handle EAGAIN and retry?
    return recv(socket, buffer, length, 0);
}

ssize_t coroutine_send(int socket, const void *buffer, size_t length)
{
    coroutine_wait_for_socket(socket, COROUTINE_RETURN_IO_WRITE);
    return send(socket, buffer, length, 0);
}

int coroutine_accept(int socket, struct sockaddr *address, socklen_t *address_len)
{
    struct coroutine_thread *th = coroutine_current_thread();

    th->flags |= COROUTINE_FLAG_WAITING;
    th->fd = socket;

    // add only one time
    coroutine_yield_with(COROUTINE_RETURN_IO); // wait for an accept

    return accept4(socket, address, address_len, SOCK_NONBLOCK);
}
