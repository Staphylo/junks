#include <coroutine/arch/x86_64.h>

void coroutine_context_dump(const struct coroutine_ctx *ctx)
{
    static const char *reg_strs[] = {
        "rdi", "rsi", "rdx", "rcx", "r8", "r9", "rax", "rbx", "r10", "r11",
        "r12", "r13", "r14", "r15", "rsp", "rbp", "rip"
    };

    uintptr_t **regs = (uintptr_t **)ctx;
    for (size_t i = 0; i < sizeof (reg_strs) / sizeof (*reg_strs); ++i) {
        log(LOG_DEBUG, "%s : %p", reg_strs[i], regs[i]);
    }
}

void coroutine_context_init(struct coroutine_ctx *ctx, void *stack,
                            size_t stack_size, void *exit_stub)
{
    ctx->rsp = (uintptr_t)stack + stack_size;

    // pushing return address with the thread exit stub
    ctx->rsp -= sizeof (ctx->rsp);
    uintptr_t *addr = (uintptr_t *)ctx->rsp;
    *addr = (uintptr_t)exit_stub;
}

void coroutine_context_make(struct coroutine_ctx *ctx, void *entry,
                            size_t args, va_list *ap)
{
    uintptr_t *addr = (uintptr_t *)ctx->rsp;
    uintptr_t return_addr = *addr;

    // allocating space for the construction of the stack
    //ctx->rsp -= args * sizeof(uintptr_t);
    ctx->rsp -= args * (sizeof(uintptr_t) + 1);
    uintptr_t *stack = (uintptr_t *)ctx->rsp;
    stack++;

    // moving return address from top of stack to it's final place
    *stack = return_addr;
    stack++;

    for (size_t i = 0; i < args; ++i) {
        uintptr_t arg = va_arg(*ap, uintptr_t);
        if (i < 6) {
            // registers are ordered by calling convention in the struct
            uintptr_t *reg = ((uintptr_t*)ctx + i);
            *reg = arg;
        }
        else {
            // pushing all parameters on the stack in the right order
            *stack = arg;
            stack++;
        }
    }

    // this address get push on the stack on context change
    // however what happens if this is just a ctx-switch (inifinite stack
    // growth?)
    ctx->rip = (uintptr_t)entry;
}

