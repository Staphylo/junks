#include <coroutine/arch/x86.h>

void coroutine_context_dump(const struct coroutine_ctx *ctx)
{
    static const char *reg_strs[] = {
        "eax", "ebx", "edx", "ecx", "esi", "edi", "esp", "ebp", "eip"
    };

    uintptr_t **regs = (uintptr_t **)ctx;
    for (size_t i = 0; i < sizeof (reg_strs) / sizeof (*reg_strs); ++i) {
        log(LOG_DEBUG, "%s : %p", reg_strs[i], regs[i]);
    }
}

void coroutine_context_init(struct coroutine_ctx *ctx, void *stack,
                            size_t stack_size, void *exit_stub)
{
    ctx->esp = (uintptr_t)stack + stack_size;
    ctx->esp -= 8;
    uintptr_t *addr = (uintptr_t *)ctx->esp;
    *addr = (uintptr_t)exit_stub;
}

void coroutine_context_make(struct coroutine_ctx *ctx, void *entry,
                            size_t args, va_list *ap)
{
    uintptr_t *addr = (uintptr_t *)ctx->esp;
    uintptr_t return_addr = *addr;

    // allocating space for the construction of the stack
    ctx->esp -= args * sizeof(uintptr_t);
    uintptr_t *stack = (uintptr_t *)ctx->esp;

    // moving return address from top of stack to it's final place
    *stack = return_addr;
    stack++;

    // pushing all parameters on the stack in the right order
    for (size_t i = 0; i < args; ++i) {
        *stack = va_arg(*ap, uintptr_t);
        stack++;
    }

    // this address get push on the stack on context change
    // however what happens if this is just a ctx-switch (inifinite stack
    // growth?)
    ctx->eip = (uintptr_t)entry;
}

