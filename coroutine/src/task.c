#define _GNU_SOURCE
#include <stdlib.h>
#include <pthread.h>

#include <coroutine/coroutine.h>
#include <coroutine/task.h>

struct worker {
    struct worker *next;
    struct worker *prev;
    pthread_t thid;
};

enum worker_action {
    WORKER_STOP,
    WORKER_PROCESS
};

struct worker_msg {
    struct worker_msg *next;
    enum worker_action action;
    union {
        struct {
            coroutine_task_f func;
            void *data;
            struct coroutine_thread *thread;
        } process;
    };
};

struct worker_msg_queue {
    struct worker_msg *head;
    struct worker_msg *tail;
    struct worker_msg *free;
    volatile size_t count;
    size_t free_count;
    pthread_cond_t cond;
    pthread_mutex_t mutex;
};

// FIXME avoid using static variable use coroutine context to store this
static __thread struct worker *workers = NULL;
static struct worker_msg_queue msg_queue;

static struct worker_msg *coroutine_task_msg_pop()
{
    struct worker_msg_queue *queue = &msg_queue;

    // waiting for a message in the message queue
    pthread_mutex_lock(&queue->mutex);
    while (queue->count == 0) {
        pthread_cond_wait(&queue->cond, &queue->mutex);
    }

    log(LOG_WARN, "count %zu", queue->count);
    struct worker_msg *msg = queue->head;
    queue->head = msg->next;
    if (msg == queue->tail)
        queue->tail = NULL;
    queue->count--;

    pthread_mutex_unlock(&queue->mutex);

    return msg;
}

static void coroutine_task_msg_collect(struct worker_msg *msg)
{
    struct worker_msg_queue *queue = &msg_queue;

    memset(msg, 0, sizeof (*msg)); // XXX optional
    pthread_mutex_lock(&queue->mutex);
    msg->next = queue->free;
    queue->free = msg;
    queue->free_count++;
    pthread_mutex_unlock(&queue->mutex);
}

static void *coroutine_task_worker_routine(void *data)
{
    struct worker *self = data;

    while (true) {
        pthread_setname_np(pthread_self(), "waiting...");
        struct worker_msg *msg = coroutine_task_msg_pop();

        if (msg->action == WORKER_STOP) {
            break;
        }

        pthread_setname_np(pthread_self(), "processing...");
        void *res = msg->process.func(msg->process.data);
        msg->process.data = res;

        // XXX this line is a race condition but since it's the only writer...
        msg->process.thread->flags &= ~(COROUTINE_FLAG_TASK);
    }

    log(LOG_NOTICE, "worker %lu died", self->thid);
    return NULL;
}

bool coroutine_task_worker_add()
{
    struct worker *worker = malloc(sizeof (struct worker));
    if (!worker) {
        log_errno("failed to allocate worker msg");
        return false;
    }

    worker->next = workers;
    worker->prev = NULL;

    if (workers)
        workers->prev = worker;
    workers = worker;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    // create thread
    int res = pthread_create(&worker->thid, &attr,
                             coroutine_task_worker_routine, worker);

    if (res != 0) {
        log_errno("failed to create new thread");
        return false;
    }

    return true;
}

static struct worker_msg *coroutine_task_msg_new()
{
    struct worker_msg_queue *queue = &msg_queue;
    struct worker_msg *msg = NULL;

    if (queue->free_count) {
        msg = queue->free;
        queue->free = msg->next;
        msg->next = NULL; // XXX Not required but safety mesure
        queue->free_count--;
    }
    else {
        msg = calloc(1, sizeof (struct worker_msg));
    }

    return msg;
};

static void coroutine_task_msg_feed(struct worker_msg *msg) {
    struct worker_msg_queue *queue = &msg_queue;

    // msg->next = NULL
    if (queue->tail)
        queue->tail->next = msg;
    else
        queue->head = msg;
    queue->tail = msg;
    queue->count++;

    pthread_cond_signal(&queue->cond);
}

void coroutine_task_worker_del()
{
    struct worker_msg *msg = coroutine_task_msg_new();
    msg->action = WORKER_STOP;
    coroutine_task_msg_feed(msg);
}

void *coroutine_task_run(coroutine_task_f func, void *data)
{
    struct coroutine_thread *self = coroutine_current_thread();
    self->flags |= COROUTINE_FLAG_TASK;

    struct worker_msg *msg = coroutine_task_msg_new();
    msg->action = WORKER_PROCESS;
    msg->process.func = func;
    msg->process.data = data;
    msg->process.thread = self;
    coroutine_task_msg_feed(msg);

    coroutine_yield_with(COROUTINE_RETURN_TASK);

    void *result = ((volatile struct worker_msg*)msg)->process.data;
    coroutine_task_msg_collect(msg);

    return result;
}

void coroutine_task_init(size_t thread_count)
{
    for (size_t i = 0; i < thread_count; ++i) {
        coroutine_task_worker_add();
    }
}

static void coroutine_task_msg_list_free(struct worker_msg *msg)
{
    while (msg) {
        struct worker_msg *tmp = msg;
        msg = msg->next;
        free(tmp);
    }
}

void coroutine_task_exit()
{
    struct worker_msg_queue *queue = &msg_queue;
    while (queue->count) {
        coroutine_task_worker_del();
    }

    coroutine_task_msg_list_free(queue->head);
    coroutine_task_msg_list_free(queue->free);
}
