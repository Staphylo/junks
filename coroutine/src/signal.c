#define _GNU_SOURCE
#include <signal.h>
#include <sys/signalfd.h>

#include <coroutine/log.h>
#include <coroutine/signal.h>

bool coroutine_signal_init(struct coroutine_signal *sig)
{
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);
    sigaddset(&mask, SIGUSR1);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
        log_errno("sigprocmask failed");
        return false;
    }

    sig->mask = mask;
    sig->signalfd = signalfd(-1, &mask, SFD_NONBLOCK);

    if (sig->signalfd < 0) {
        log_errno("signalfd creation failed");
        return false;
    }

    return true;
}

