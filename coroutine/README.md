Coroutine
=========

This project is a coroutine framework in C.
Coroutines are also called userland threads / green threads.

The differences comparing to native threads are
  - own scheduler
  - cooperative, not preemptive therefore a blocking operation freeze
    everything (a task pool can be used for CPU intensive operation / blocking)
  - sequential execution instead of parallel
  - best for IO bound tasks
  - internally asynchronous IO but synchronous in the programming model
  - no synchronisation required
  - no limit on thread number (in comparison)
  - blocking is not an option (freeze everything) but require a task pool

Currently handled features
==========================

 - Coroutine creation using lambdas or function pointer
 - Round Robin without preemption

see `example/` and `tests/` for some examples

Features in development
=======================

 - Wait for n ms
    - missing blocking signalfd when there is no more schedulable entities
 - async io for read/write
    - use libaio kernel API with io\_setup
 - task pool to be able to run a task in a thread and wait for the result
    - missing blocking eventfd when a task end and there are no more thread
      to schedule

Things to do
============

 - Add a synchronisation mechanism using eventfd
 - Do a real scheduler and not a dummy and fixed one
   - refactor thread organisation
 - More tests
   - mixing features mostly
 - Optimisation

Architectures
=============

This project currently support the following architectures

 - x86\_64
 - x86

Adding an architecture is quite easy, since only the context switching and
the context creation must be implemented (see src/arch/\* and
include/context.h)

Building
========

To build the coroutine library just do

    make

To build the tests run

    make tests

To run the testsuite

    make check

To change the architecture

    make arch-<arch>

Misc
====

Beerware license
