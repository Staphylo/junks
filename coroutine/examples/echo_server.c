#include <coroutine/coroutine.h>
#include <coroutine/wrappers.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

struct client {
    int socket;
    struct sockaddr_in addr;
};

int create_server(const char *ip, uint16_t port)
{
    int s = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;

    if (s < 0) {
        fprintf(stderr, "Failed to create a socket\n");
        return -1;
    }

    int opt = 1;
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) {
        fprintf(stderr, "Could not set SO_REUSEADDR\n");
        return -1;
    }

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(ip);
    addr.sin_port = htons(port);

    if (bind(s, (struct sockaddr *)&addr, sizeof (addr)) < 0) {
        fprintf(stderr, "Could not bind socket\n");
        return -1;
    }

    if (listen(s, 10) < 0) {
        fprintf(stderr, "Could not listen socket\n");
        return -1;
    }

    return s;
}

int main(int argc, char *argv[])
{
    coroutine_init();

    uint16_t port = 4444;
    if (argc > 1) {
        port = atoi(argv[1]);
    }
    int s = create_server("0.0.0.0", port);
    if (s < 0)
        return 1;

    coroutine((int server) {
        while (true) {
            struct client *c = malloc(sizeof (struct client));
            socklen_t len = sizeof(c->addr);
            c->socket = coroutine_accept(server, (void*)&c->addr, &len);
            if (c->socket == -1) {
                free(c);
                return;
            }

            printf("[%d][%s:%d] new client\n", c->socket, inet_ntoa(c->addr.sin_addr), ntohs(c->addr.sin_port));

            coroutine((struct client *c) {
                while (true) {
                    char buffer[2048];
                    ssize_t res = coroutine_recv(c->socket, buffer, sizeof (buffer));
                    if (res <= 0) {
                        printf("[%d][%s:%d] recv %zd %d\n", c->socket, inet_ntoa(c->addr.sin_addr), ntohs(c->addr.sin_port), res, errno);
                        perror("recv");
                        break;
                    }
                    printf("[%d][%s:%d] sending %zd bytes\n", c->socket, inet_ntoa(c->addr.sin_addr), ntohs(c->addr.sin_port), res);
                    coroutine_send(c->socket, buffer, res);
                }
                close(c->socket);
                free(c);
            }, c);
        }
    }, s);
    coroutine_run();
    return 0;
}
