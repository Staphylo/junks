#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// TODO
// clang support
// forward parameters to the caller
// - using eabi to put them on registers and stack
// preemptive mode with 'setitimer' 'sigalarm'
// be preemptive for the wait method

#include <coroutine/coroutine.h>
#define sizeofarray(Array) ( sizeof(Array) / sizeof(*array) )

struct functestdata {
    int id;
};

struct functestdata *functestdata_make(int id) {
    struct functestdata *data = malloc(sizeof (struct functestdata));
    data->id = id;
    return data;
}

void coroutine_test(void *data)
{
    struct functestdata *d = data;
    printf("test%d: begin\n", d->id);
    coroutine_yield();
    printf("test%d: end\n", d->id);
}

// pthread style
void test_funcs()
{
    coroutine_init();
    coroutine_create(coroutine_test, functestdata_make(1));
    coroutine_create(coroutine_test, functestdata_make(2));
    coroutine_run();
}

// pthread style but using lambdas
void test_lambda()
{
    coroutine_init();

    coroutine_f test_1 = colambda({
        puts("test1: this");
        coroutine_yield();
        puts("test1: a");
        coroutine_yield();
        puts("test1: for");
        coroutine_yield();
        puts("test1: scheduler");
    });
    coroutine_create(test_1, NULL);


    coroutine_f test_2 = colambda({
        puts("test2: is");
        coroutine_yield();
        puts("test2: test");
        coroutine_yield();
        puts("test1: the");
    });
    coroutine_create(test_2, NULL);

    coroutine_run();
}

void test_coroutine()
{
    coroutine_init();

    /*
#define $$$ },{
    coroutine_create_full({
        int a; int b;
    },{
        $$$
        .a = 12, .b = 30
    },{
        $$$
        printf("%d - %d\n", arg.a, arg.b);
    });*/

    coroutine((int a, int b, int c) {
        printf("params: a = %d\n", a);
        coroutine_yield();
        printf("params: b = %d\n", b);
        coroutine_yield();
        printf("params: c = %d\n", c);
    }, 30, 12, 1337);

    coroutine((const char *filename) {
        printf("file: open(%s)\n", filename);
        int fd = open(filename, O_RDONLY, 0);
        printf("file: open => %d\n", fd);
        if (fd < 0)
            return;
        char data[4096];
        printf("file: read(%d)\n", fd);
        ssize_t res = coroutine_read(fd, data, sizeof (data));
        printf("file: read => %zd\n", res);
        if (res > 0) {
            printf("file: write(%d)\n", fd);
            coroutine_write(STDOUT_FILENO, data, res);
        }
    }, "Makefile");

    coroutine(() {
        puts("useless: wasting 1 context switch");
    });

    const char *array[] = {
        "foo",
        "bar",
        "baz",
        "cul"
    };

    coroutine((int argc, char **argv) {
        for (int i = 0; i < argc; ++i) {
            coroutine((const char *name) {
                printf("%s\n", name);
            }, argv[i]);
        }
        puts("coroutine added");
    }, sizeofarray(array), array);


    /*coroutine(() {*/
    /*    coroutine_wait_us(200000);*/
    /*    puts("wait2: timeout elapsed");*/
    /*});*/

    /*coroutine(() {*/
    /*    coroutine_wait_ms(100);*/
    /*    puts("wait1: timeout elapsed");*/
    /*});*/

    coroutine_run();
}

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    test_funcs();
    /*test_lambda();*/
    /*test_coroutine();*/

    return 0;
}
