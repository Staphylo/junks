#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <coroutine/coroutine.h>
#include <pthread.h>

static void bench_coroutine(int argc, char **argv)
{
    coroutine_init();

    int size = 33; // md5sum + \n

    for (int i = 1; i < argc; ++i) {
        coroutine((const char *filename, size_t size) {
            ssize_t len;
            char buffer[size];
            char outfilename[4096];

            strcpy(outfilename, filename);
            strcat(outfilename, ".out.co");

            // coroutine open using io_setup
            int fdin = open(filename, O_RDONLY);
            int fdout = open(outfilename, O_WRONLY | O_CREAT | O_TRUNC, 0644);

            if (!fdout || !fdin)
                return;

            while ((len = coroutine_read(fdin, buffer, size)) > 0) {
                char *begin = buffer;
                char *end = buffer + len - 1 - 1; // -1 for the \n
                while (begin < end) {
                    char tmp = *begin;
                    *begin = *end;
                    *end = tmp;
                    begin++;
                    end--;
                }

                coroutine_write(fdout, buffer, len);
            }

        }, argv[i], size);
    }

    coroutine_run();
}

struct thread_param {
    const char *filename;
    size_t size;
};

static void *thread_compute(void *data)
{
    struct thread_param *params = data;
    ssize_t len;
    char buffer[params->size];
    char outfilename[4096];

    strcpy(outfilename, params->filename);
    strcat(outfilename, ".out.co");

    int fdin = open(params->filename, O_RDONLY);
    int fdout = open(outfilename, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    if (!fdout || !fdin)
        return NULL;

    while ((len = read(fdin, buffer, params->size)) > 0) {
        char *begin = buffer;
        char *end = buffer + len - 1 - 1; // -1 for the \n
        while (begin < end) {
            char tmp = *begin;
            *begin = *end;
            *end = tmp;
            begin++;
            end--;
        }

        write(fdout, buffer, len);
    }
    return NULL;
}

static void bench_threads(int argc, char **argv)
{
    pthread_t threads[argc - 1];

    for (int i = 1; i < argc; ++i) {
        struct thread_param *params = malloc (sizeof (struct thread_param));
        params->filename = argv[i];
        params->size = 33;
        pthread_create(threads + i - 1, NULL, thread_compute, params);
    }

    for (int i = 0; i < argc - 1; ++i)
        pthread_join(threads[i], NULL);
}

static void how_long(struct timeval *begin, struct timeval *end)
{
    double elapsed;
    elapsed = (end->tv_sec - begin->tv_sec) * 1000.0;      // sec to ms
    elapsed += (end->tv_usec - begin->tv_usec) / 1000.0;   // us to ms
    printf("time spent %lf\n", elapsed);
}

int main(int argc, char *argv[])
{
    struct timeval begin;
    struct timeval end;


    gettimeofday(&begin, NULL);
    bench_coroutine(argc, argv);
    gettimeofday(&end, NULL);
    how_long(&begin, &end);

    gettimeofday(&begin, NULL);
    bench_threads(argc, argv);
    gettimeofday(&end, NULL);
    how_long(&begin, &end);

    /*bench_pthread();*/
    return 0;
}
