#ifndef COROUTINE_SIGNAL_H_
# define COROUTINE_SIGNAL_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>

struct coroutine_signal
{
    sigset_t mask;
    int signalfd;
};

bool coroutine_signal_init(struct coroutine_signal *sig);

__END_DECLS

#endif /* !COROUTINE_SIGNAL_H_ */
