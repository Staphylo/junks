#ifndef COROUTINE_THREADS_H_
# define COROUTINE_THREADS_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>

# include <sys/time.h>
# include <time.h>
# include <aio.h>

# include <coroutine/context.h>

// can be user defined
# ifndef COROUTINE_THREAD_ALLOC_SIZE
#  define COROUTINE_THREAD_ALLOC_SIZE 10
# endif

// can be user defined (runtime better?)
# ifndef COROUTINE_STACK_SIZE
#  define COROUTINE_STACK_SIZE (64 * 1024)
# endif

# define COROUTINE_FLAG_EMPTY       0
# define COROUTINE_FLAG_USED        0x1
# define COROUTINE_FLAG_WAITING     0x2
# define COROUTINE_FLAG_TIMER       0x4
# define COROUTINE_FLAG_TASK        0x8

# ifdef __clang__
typedef void (^coroutine_f)();
# else
typedef void (*coroutine_f)();
#endif

typedef struct coroutine_thread
{
    volatile uint32_t flags;
    struct coroutine_ctx ctx;
    int fd;
    struct timespec wait;
    size_t stack_size;
    void *stack;
    coroutine_f func;
    void *data;
} cthread_s;

struct coroutine_threads
{
    // number of threads
    size_t count;
    // size of the array
    size_t size;
    // last cell occupied
    size_t last;
    // active threads
    size_t active;
    // array of coroutine
    struct coroutine_thread *threads;
};

typedef size_t cthread_i;

static inline bool coroutine_thread_ready(struct coroutine_thread *thread)
{
    return (thread->flags & COROUTINE_FLAG_USED) &&
           !(thread->flags & COROUTINE_FLAG_WAITING) &&
           !(thread->flags & COROUTINE_FLAG_TIMER) &&
           !(thread->flags & COROUTINE_FLAG_TASK);
}

bool coroutine_threads_init(struct coroutine_threads *thread);
struct coroutine_thread *coroutine_threads_new(struct coroutine_threads *threads);
struct coroutine_thread *coroutine_threads_next(struct coroutine_threads *threads, struct coroutine_thread *current);
void coroutine_threads_remove(struct coroutine_threads *threads, struct coroutine_thread *th);
void coroutine_threads_dump(struct coroutine_threads *threads);

__END_DECLS

#endif /* !COROUTINE_THREADS_H_ */
