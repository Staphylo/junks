#ifndef COROUTINE_H_
# define COROUTINE_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>
# include <coroutine/threads.h>
# include <coroutine/wrappers.h>
# include <coroutine/signal.h>
# include <coroutine/timer.h>
# include <coroutine/task.h>

enum coroutine_return {
    COROUTINE_RETURN_OK = 1,
    COROUTINE_RETURN_EXIT,
    COROUTINE_RETURN_YIELD,
    COROUTINE_RETURN_IO,
    COROUTINE_RETURN_IO_READ,
    COROUTINE_RETURN_IO_WRITE,
    COROUTINE_RETURN_TIMER,
    COROUTINE_RETURN_TASK,
};

# define COROUTINE_EPOLL_QUEUE  42

struct coroutine_io
{
    int epollfd;
};

struct coroutine_manager
{
    // thread of the scheduler (current thread)
    struct coroutine_thread scheduler;
    // signal handling information
    struct coroutine_signal signal;
    // io handling information
    struct coroutine_io io;
    // bool to stop the coroutine system
    struct coroutine_threads threads;
    // current item being run
    struct coroutine_thread *current;
    // boolean checked on each scheduling
    bool is_running;
};

# define colambda(Body) (coroutine_f)({ void $(void *data __unused) Body; $; })

#if 0
#define coroutine(Body, ...) \
    do { \
        coroutine_f tmp = colambda(Body); \
        coroutine_add(tmp, ##__VA_ARGS__); \
    } while(0)
#endif

#define VA_NARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, \
                      _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, \
                      _25, _26, _27, _28, _29, _30, _31, _32, _N, ...) \
                      _N
#define VA_NARGS(...) VA_NARGS_IMPL(X,##__VA_ARGS__, 32, 31, 30, 29, 28, 27, \
                                    26, 25, 24, 23, 22, 21, 20, 19, 18, 17, \
                                    16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, \
                                    5, 4, 3, 2, 1, 0)

// this doesn't work with types larger than sizeof (void *)
# ifdef __clang__
#  define DECLARE_NESTED_FUNCTION(Lambda) (^ Lambda)
# else
#  define DECLARE_NESTED_FUNCTION(Lambda) ({ void $ Lambda; $; })
# endif

# define coroutine_add(Func, ...) \
    coroutine_vadd(Func, VA_NARGS(__VA_ARGS__), ## __VA_ARGS__);

# define coroutine(Lambda, ...) \
{ \
    coroutine_f tmp = DECLARE_NESTED_FUNCTION(Lambda); \
    coroutine_add(tmp, ## __VA_ARGS__); \
}

# define coroutine_every(Every, Lambda, ...)

static inline struct coroutine_manager *coroutine_instance() {
    extern struct coroutine_manager coroutine_manager;
    return &coroutine_manager;
}

static inline struct coroutine_thread *coroutine_current_thread() {
    struct coroutine_manager *cm = coroutine_instance();
    return cm->current;
}

bool coroutine_init();
void coroutine_exit();
void coroutine_run();
void coroutine_yield_with(int status);
void __noreturn coroutine_end(void);
void coroutine_create(coroutine_f func, void *data);
void coroutine_vadd(coroutine_f func, size_t args, ...);

__inline void coroutine_yield() {
    coroutine_yield_with(COROUTINE_RETURN_YIELD);
}

__END_DECLS

#endif /* !COROUTINE_H_ */
