#ifndef COROUTINE_TYPES_H_
# define COROUTINE_TYPES_H_

# include <sys/types.h>
# include <stdbool.h>
# include <stdint.h>
# include <inttypes.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#endif /* !COROUTINE_TYPES_H_ */
