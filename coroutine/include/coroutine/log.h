#ifndef LOG_H_
# define LOG_H_

# include <stdio.h>
# include <errno.h>
# include <string.h>

# define XSTR(S) STR(S)
# define STR(S) #S

# define LOG_ERROR  ("error",  10, "31")
# define LOG_WARN   ("warn",   20, "33")
# define LOG_NOTICE ("notice", 30, "34")
# define LOG_DEBUG  ("debug",  40, "35")

# define LOG_TYPE_NAME(Type) LOG_TYPE_EXTRACT_NAME Type
# define LOG_TYPE_EXTRACT_NAME(Name, Level, Color) Name

# define LOG_TYPE_LEVEL(Type) LOG_TYPE_EXTRACT_LEVEL Type
# define LOG_TYPE_EXTRACT_LEVEL(Name, Level, Color) Level

# define LOG_TYPE_COLOR(Type) LOG_TYPE_EXTRACT_COLOR Type
# define LOG_TYPE_EXTRACT_COLOR(Name, Level, Color) Color

# define __log(Type, Fmt, End, ...) \
    if (DEBUG_LEVEL >= LOG_TYPE_LEVEL(Type)) \
        fprintf(stderr, "\033[" LOG_TYPE_COLOR(Type) ";1m[" __FILE__ ":" XSTR(__LINE__) "] " \
                        LOG_TYPE_NAME(Type) ": " Fmt "\033[0m" End, ## __VA_ARGS__)

# define log(Type, Fmt, ...) \
    __log(Type, Fmt, "\n", ## __VA_ARGS__)

# define debug(Fmt, ...) \
    __log(LOG_DEBUG, Fmt, "\n", __VA_ARGS__)

# define log_errno(Fmt, ...) \
    __log(LOG_ERROR, Fmt, " (%s)\n", ## __VA_ARGS__ , strerror(errno))

# define logn(Type, Fmt, ...) \
    __log(Type, Fmt, "", ## __VA_ARGS__)

# ifndef DEBUG_LEVEL
#  define DEBUG_LEVEL 40
# endif

#endif /* LOG_H_ */

