#ifndef COROUTINE_DECLS_H_
# define COROUTINE_DECLS_H_

# include <assert.h>
# include <sys/cdefs.h>

# define __packed __attribute__((packed))
# define __unused __attribute__((unused))
# define __inline inline __attribute__((always_inline))
# define __constructor __attribute__((constructor))
# define __destructor __attribute__((destructor))
# define __init_priority(Prio) __attribute__((init_priority(Prio)))
# define __noreturn __attribute__((noreturn))

// XXX add other useful stuff

#endif /* !COROUTINE_DECLS_H_ */
