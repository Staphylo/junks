#ifndef COROUTINE_TIMER_H_
# define COROUTINE_TIMER_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>

// wait for at least this period of time
// the elapsed delay won't be seen until the scheduler check the flags
void coroutine_wait_ns(uint64_t ns);

// TODO: handle overflow
static __inline void coroutine_wait_ms(uint64_t ms) {
    coroutine_wait_ns(ms * 1000000);
}

static __inline void coroutine_wait_us(uint64_t us) {
    coroutine_wait_ns(us * 1000);
}

__END_DECLS

#endif /* !COROUTINE_TIMER_H_ */
