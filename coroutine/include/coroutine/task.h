#ifndef COROUTINE_TASK_H_
# define COROUTINE_TASK_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>

typedef void *(*coroutine_task_f)(void *);

# define coroutine_task(Lambda, ...)

void *coroutine_task_run(coroutine_task_f func, void *data);
size_t coroutine_task_worker_count();
void coroutine_task_worker_count_set(size_t count);
void coroutine_task_init();
void coroutine_task_exit();

__END_DECLS

#endif /* !COROUTINE_THREADS_H_ */
