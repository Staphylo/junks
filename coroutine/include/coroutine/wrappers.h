#ifndef COROUTINE_WRAPPERS_H_
# define COROUTINE_WRAPPERS_H_

# include <coroutine/decls.h>

__BEGIN_DECLS

# include <coroutine/types.h>

// FIXME: rename this file as io.h
// XXX This header is just an helper for users that provide open(2)
# include <fcntl.h>
// XXX This one for socklen_t
# include <sys/socket.h>

ssize_t coroutine_read(int fd, void *buf, size_t size);
ssize_t coroutine_write(int fd, const void *buf, size_t size);
ssize_t coroutine_send(int socket, const void *buffer, size_t length);
ssize_t coroutine_recv(int socket, void *buffer, size_t length);
int coroutine_accept(int socket, struct sockaddr *address,
                     socklen_t *address_len);

__END_DECLS

#endif /* !COROUTINE_WRAPPERS_H_ */
