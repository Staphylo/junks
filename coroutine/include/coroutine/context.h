#ifndef CONTEXT_H_
# define CONTEXT_H_

# if defined(__linux__) && defined(__x86_64__)
#  include <coroutine/arch/x86_64.h>
# elif defined(__linux__) && defined(__i386__)
#  include <coroutine/arch/x86.h>
# else
#  error "No supported architecture found see: context.h"
# endif


#endif /* !CONTEXT_H_ */
