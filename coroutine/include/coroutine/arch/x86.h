#ifndef COROUTINE_ARCH_X86_H_
# define COROUTINE_ARCH_X86_H_

# include <stdarg.h>
# include <stdint.h>
# include <sys/types.h>

# include <coroutine/log.h>

struct coroutine_ctx
{
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
    uint32_t esi;
    uint32_t edi;
    uint32_t esp;
    uint32_t ebp;
    uint32_t eip;
};

void coroutine_context_dump(const struct coroutine_ctx *ctx);
int coroutine_context_save(struct coroutine_ctx *ctx);
int coroutine_context_restore(const struct coroutine_ctx *ctx, int code);
void coroutine_context_init(struct coroutine_ctx *ctx, void *stack,
                            size_t stack_size, void *exit_stub);
void coroutine_context_make(struct coroutine_ctx *ctx, void *entry,
                            size_t args, va_list *ap);

#endif /* !COROUTINE_ARCH_X86_H_ */
