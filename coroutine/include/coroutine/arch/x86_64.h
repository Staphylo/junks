#ifndef COROUTINE_ARCH_X86_64_H_
# define COROUTINE_ARCH_X86_64_H_

# include <stdarg.h>
# include <stdint.h>
# include <sys/types.h>

# include <coroutine/log.h>

struct coroutine_ctx
{
    uint64_t rdi;
    uint64_t rsi;
    uint64_t rdx;
    uint64_t rcx;
    uint64_t r8;
    uint64_t r9;
    uint64_t rax;
    uint64_t rbx;
    uint64_t r10;
    uint64_t r11;
    uint64_t r12;
    uint64_t r13;
    uint64_t r14;
    uint64_t r15;
    uint64_t rsp;
    uint64_t rbp;
    uint64_t rip;
};

void coroutine_context_dump(const struct coroutine_ctx *ctx);
int coroutine_context_save(struct coroutine_ctx *ctx);
int coroutine_context_restore(const struct coroutine_ctx *ctx, int code);
void coroutine_context_init(struct coroutine_ctx *ctx, void *stack,
                            size_t stack_size, void *exit_stub);
void coroutine_context_make(struct coroutine_ctx *ctx, void *entry,
                            size_t args, va_list *ap);

#endif /* !COROUTINE_ARCH_X86_64_H_ */
